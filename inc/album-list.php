<?php
/**
* List albums
*/
echo "<h3>Albums</h3>\n<p>";
$where = '';
$params = array();
if ( isset($search) ) {
  $where = "(artist ~* :search OR album ~* :search OR title ~* :search OR path_name ~* :search) ";
  $params[':search'] = $search;
}
if ( isset($altr) ) {
  $altr = trim($altr);
  $where .= ($where == ''? '' : ' AND ');
  $where .= '(album ~ :album_match)';
  $params[':album_match'] = '^'.$altr;
}
if ( $where != '' ) $where = 'WHERE '. $where;
$sql = 'SELECT distinct ON (lower(album)) album FROM tracks '.$where.' ORDER BY lower(album)';
$qry = new AwlQuery( $sql, $params );

if ( $qry->Exec('album') && $qry->rows() > 0 ) {
  while ( $album = $qry->Fetch() ) {
    $display = htmlspecialchars($album->album);
    if ( trim($display) == "" ) $display = "&laquo;unknown&raquo;";
    echo " <a href=\"?type=album&l=" . urlencode($album->album) . "$letter_get\" class=\"album\">$display</a>\n";
  }
}

