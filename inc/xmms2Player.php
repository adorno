<?php

putenv('COLUMNS=500');
putenv('XMMS_PATH=tcp://127.0.0.1:9667/');

error_log( "Using XMMS backend: ". $c->daemon_type );
class xmms2Connection {
  var $active;            // Once it actually is...
  var $xmms2;
  var $status;

  /**
  * Create a minimally initialised connection object
  */
  function xmms2Connection() {
    $this->active = false;
    $this->xmms2 = false;
    $this->status = false;
  }

  /**
  * Connect to the remote xmms2
  *  - a complete fake.
  */
  function Connect() {
    global $c;

    $this->xmms2 = true;
    $this->active = true;
  }

  /**
  * Do a question/response pair with the daemon
  */
  function OldDaemon( $say ) {
    if ( ! $this->active ) $this->Connect();

    $command = 'LC_ALL=en_NZ.UTF8 xmms2 ' . $say . ' 2>&1';
    $output = array();
    $retval = 0;
    error_log("adorno: DBG: XMMS2CMD: >>>$command<<<");
    exec( $command, &$output, &$retval );

    if ( preg_match( '#Could not connect to xmms2d#i', $output[0] ) ) {
      exec( 'xmms2-launcher', &$output, &$retval );
      error_log("adorno: DBG: Launching XMMS2 daemon: ($retval) - " . $output[0]);
      exec( 'xmms2 playlist type Default queue', &$output, &$retval );
      error_log("adorno: DBG: Launching XMMS2 daemon: ($retval) - " . $output[0]);
      $output = array();
      $retval = 0;
      exec( $command, &$output, &$retval );
    }
    return $output;
  }

  /**
  * Do a question/response pair with the daemon via nyxmms2
  */
  function Daemon( $say ) {
    if ( ! $this->active ) $this->Connect();

    $command = 'LC_ALL=en_NZ.UTF8 nyxmms2 ' . $say . ' 2>&1';
    $output = array();
    $retval = 0;
    error_log("adorno: DBG: NYCLICMD: >>>$command<<<");
    exec( $command, &$output, &$retval );

    if ( preg_match( '#Could not connect to #i', $output[0] ) ) {
      exec( 'xmms2-launcher', &$output, &$retval );
      error_log("adorno: DBG: Launching XMMS2 daemon: ($retval) - " . $output[0]);
      exec( 'xmms2 playlist type Default queue', &$output, &$retval );
      error_log("adorno: DBG: Launching XMMS2 daemon: ($retval) - " . $output[0]);
      $output = array();
      $retval = 0;
      exec( $command, &$output, &$retval );
    }
    return $output;
  }

  /**
  * Query the xmms2 about it's current status
  */
  function UpdateStatus( $force = false ) {
    if ( ! $force && is_array($this->status) ) return;
    $this->status = array();
    $status = $this->Daemon( "status" );
    $this->status['state'] = preg_replace( '/:.*$/', '', $status[0]);
    $status = $this->Daemon( "info" );
    foreach( $status AS $k => $v ) {
      list( $key, $value ) = preg_split( '/ = /', $v, 2);
      if ( preg_match( '/\b(url|laststarted|duration|mime|album|artist|title)\s*$/', $key, $matches ) ) {
        $key = $matches[1];
      }
      if ( preg_match( '/\%[0-9a-f][0-9a-f]/i', $value ) ) {
        $value = rawurldecode($value);
      }
      // printf( "<p>status[%s] = [[%s]]</p>\n", $key, $value );
      $this->status[$key] = $value;
    }
  }


  /**
  * Encode a filename to escape spaces & stuff.
  */
  function EncodeFileName( $track ) {
    if ( preg_match( '#[a-z]{3,7}://#', $track ) ) {
      return $track;
    }
    $encoded = str_replace( chr(92), '\\\\', $track);
    $encoded = str_replace( '"', '\\"', $track);
    // $encoded = str_replace( "'", "\\'", $track);
    // $encoded = preg_replace( '/([\\\'"#&;`|*?~<>^\(\)[]{}$ ])/', '\\\\\1', $encoded);
    # $encoded = escapeshellarg($track);
    $encoded = '"file://' . $encoded . '"';
    error_log( "Trying to play: $encoded" );
    return $encoded;
  }


  /**
  * Play a track.  Xmms2 is OK if we tell it to play when it is already.
  */
  function Play( $track ) {
    global $c;

    $this->UpdateStatus( ($this->status['state'] == 'Stopped') );

    // Replace ' with '' so escapeshellcmd reliably ignores it, then with \' afterwards
    $this->Daemon("add ". $this->EncodeFileName($track));
    if ( $this->status['state'] == 'Stopped' ) {
      $this->Daemon("next");
      $this->Daemon("play");
    }
    else
      $this->Daemon("play");
  }


  /**
  * Move a track in the playlist
  */
  function MoveTrack( $index, $offset ) {
    if ( $index < 1 || ($index + $offset) < 1 ) return;  // Can't move playing track

    $this->Daemon( sprintf("move %d %d", $index, $index + $offset ) );
  }


  /**
  * Query the xmms2 about it's current playlist and position
  */
  function GetCurrent() {
    global $c;

    $this->UpdateStatus();
    $songnow = (object) array();
    $songnow->started = date( 'Y-m-d H:i:s', $this->status['laststarted'] );
    $songnow->track = str_replace( 'file://', '', $this->status['url'] );
    $songnow->duration = intval($this->status['duration']) / 1000;
    $songnow->finishing = date( 'Y-m-d H:i:s', $this->status['laststarted'] + $songnow->duration);
    $songnow->state   = $this->status['state'];

    // printf( "<p>Song now is: %s started %s, finishes %s</p>\n", $songnow->track, $songnow->started, $songnow->finishing );

    return $songnow;
  }

  /**
  * Query the xmms2 about it's current playlist and position
  */
  function GetQueue() {
    global $c;

    $this->UpdateStatus();

    $this->queue = array();

    $filename = "";
    $pos = 0;

    $playlist = $this->Daemon( "list" );
    foreach( $playlist AS $k => $v ) {

      $filename = $v;
      if ( preg_match( '{@@(.+)@@}', $v, $matches ) ) $filename = $matches[1];
//       echo "<p>$filename</p>\n";
        if ( substr($filename,0,7) == 'file://' ) {
          $filename = substr( $filename, 7 );
          $filename = urldecode( $filename );
          $this->queue[] = $filename;
        }
    }

    return $this->queue;
  }

}

$GLOBALS["xmms2"] = new xmms2Connection();





/******************************************************************
* The actual API the web interface calls is then very simple...
******************************************************************/

/**
* Queue a file for playing
*/
function daemon_play_track( $path ) {
  global $xmms2;
  error_log("adorno: DBG: Trying to play '$path'");
  $xmms2->Play( $path );
}


/**
* Get a list of the files currently queued for the future
*/
function daemon_get_queue() {
  global $xmms2;
  $q = $xmms2->GetQueue();
  return $q;
}


/**
* Get the currently playing track and it's starting time
*/
function daemon_current_track() {
  global $xmms2;
  return $xmms2->GetCurrent();
}


/**
* Get the currently playing track and it's starting time
*/
function daemon_other_command( $action, $track ) {
  global $xmms2;
  switch( $action ) {
    case 'resume':
      $xmms2->Daemon('play');
      break;
    case 'pause':
      $xmms2->Daemon($action);
      break;
    case 'next':
      $xmms2->Daemon($action);
      break;
    default:
      error_log("adorno: WARNING: Unsupported command '$action'" );
      $xmms2->Daemon($action);
      break;
  }

  return true;
}

/**
* Get the currently playing track and it's starting time
*/
function daemon_move( $index, $offset ) {
  global $xmms2;
  return $xmms2->MoveTrack($index,$offset);
}


