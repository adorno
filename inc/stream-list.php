<?php
/**
* List Streams
*/
echo "<h3>Streams</h3>\n<p>";
$where = '';
$params = array();
if ( isset($search) ) {
  $where = "(stream_url ~* :search OR playlist_url ~* :search OR genre ~* :search OR description ~* :search) ";
  $params[':search'] = $search;
}
if ( isset($altr) ) {
  $altr = trim($altr);
  $where .= ($where == ''? '' : ' AND ');
  $where .= '(description ~ :match)';
  $params[':match'] = '^'.$altr;
}
if ( $where != '' ) $where = 'WHERE '. $where;
$sql = 'SELECT * FROM streams '.$where.' ORDER BY lower(description)';
$qry = new AwlQuery( $sql, $params );

if ( $qry->Exec('album') && $qry->rows() > 0 ) {
  while ( $stream = $qry->Fetch() ) {
    $display = htmlspecialchars($stream->description);
    if ( $display == '' ) $display = htmlspecialchars($stream->stream_url);
    $title = htmlspecialchars($stream->stream_url);
    if ( trim($display) == "" ) $display = "&laquo;unknown&raquo;";
    echo " <a href=\"?type=stream&url=" . urlencode($stream->stream_url) . "$letter_get\" class=\"stream\" title=\"$title\">$display</a>\n";
  }
}

