<?php
class ALink {
  var $target;
  var $content;
  var $help;
  var $template;

  function ALink( $target, $content, $help="", $template="" ) {
    $this->target = $target;
    $this->content = $content;
    $this->help = $help;
    $this->template = $template;
    return $this;
  }

  function Render( $class="" ) {
    $title = ( $this->help == "" ? "" : " title=\"$this->help\"");
    $class = ( $class == "" ? "" : " class=\"$class\"");
    if ( $this->template == "" )
      $this->template = '<a href="%s"%s%s>%s</a>';
    return sprintf( $this->template, $this->target, $class, $title, $this->content );
  }
}


class LinkSet {
  var $template;
  var $class;

  function LinkSet( $template, $class ) {
    $this->template = $template;
    $this->class = $class;
  }

  function Link( $target, $content, $help = "" ) {
    $l = new ALink( $target, $content, $help, $this->template );
    return $l->Render($this->class);
  }
}

?>