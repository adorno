<?php

error_log( "Using VLC backend: ". $c->daemon_type );
class vlcConnection {
  var $active;            // Once it actually is...
  var $vlc;
  var $status;

  /**
  * Create a minimally initialised connection object
  */
  function vlcConnection() {
    $this->active = false;
    $this->vlc = false;
    $this->status = array();
  }

  /**
  * Connect to the remote vlc
  */
  function Connect() {
    global $c;

    if ( ! $this->vlc ) {
      $this->vlc = stream_socket_client($c->vlc['connection'], $errno, $errstr, 1);
      if ( ! $this->vlc ) {
        error_log( sprintf( "vlcPlayer: ERROR: Failed to connect to '%s'.  Error %d: %s", $c->vlc['connection'], $errno, $errstr) );
        $this->vlc = 'connection failed';
        return;
      }
      error_log( "vlcPlayer: Connected to ".$c->vlc['connection'] );
      $this->active = true;
    }
  }

  /**
  * Do a question/response pair with the daemon
  */
  function Daemon( $say, $end_pattern = 'command' ) {
    if ( ! $this->active ) $this->Connect();

    $result = (object) array( 'text' => "" );
    if ( ! $this->active ) return $result;

    if ( $end_pattern == 'command' ) {
      $command = preg_replace( '/\s.*$/', '', $say);
      $end_pattern = "^$command: returned (\d+) \((.+)\)";
    }

    fwrite($this->vlc, $say . "\n" );
    stream_set_timeout($this->vlc, 0, 50000);

    while ( ! feof($this->vlc) ) {
      $line = fgets($this->vlc, 8192);
//      echo "<p>$line</p>\n";
      $result->text .= $line;
      if ( preg_match( "/$end_pattern/", $line, $matches) ) {
        $result->matches = $matches;
        break;
      }
    }

    error_log( sprintf("vlcPlayer: Sent '%s' and result was '%s'", $say, $result->text) );

    return $result;
  }


  /**
  * Query the vlc for a simple command which just returns a single data value
  */
  function DaemonSimple( $command ) {
    $response = $this->Daemon( $command, '^(\S+)\s*$' );
    return $response->matches[1];
  }


  /**
  * Query the vlc about it's current status
  */
  function UpdateStatus() {
    $this->status = array();

    $response = $this->Daemon( "status" );
    $lines = split( "\n", $response->text );
    foreach( $lines AS $k => $v ) {
      $v = str_replace( 'status change: ( ', '', $v);
      $v = preg_replace( '/ \)\s*$/', '', $v );
      list( $key, $value ) = preg_split( '/:\s*/', $v, 2);
      $this->status[$key] = $value;
    }

    $this->status['time'] = $this->DaemonSimple( "get_time" );
    $this->status['started'] = date( 'Y-m-d H:i:s', time() - $this->status['time']);
    $this->status['length'] = $this->DaemonSimple( "get_length" );

    $this->status['state'] = ( $this->DaemonSimple("is_playing") == "1" ? "play" : "stop" );
  }


  /**
  * Play a track.  If the current status is 'stop' then we
  * first clear the playlist and after tell vlc to play.
  */
  function Play( $track ) {
    global $c;

    $result = $this->Daemon( sprintf('enqueue %s', $track) );

    $this->UpdateStatus();
    if ( $this->status['state'] == 'stop' ) {
      $this->GetQueue();
      $this->Daemon( sprintf('goto %d', count($this->playlist) - 1 ) );
    }

  }


  /**
  * Query the vlc about it's current playlist and position
  */
  function GetCurrent() {
    global $c;

    $this->UpdateStatus();
    $songnow = (object) array();
    if ( $this->status['state'] == 'stop' ) return $songnow;

    $response = $this->Daemon( "playlist" );
    $current = split( "\n", $response->text );
    foreach( $current AS $k => $v ) {
      if ( preg_match( '/^\|\*.*   (.*)\|([^\|]*)\|\s*$/', $v, $matches ) ) {
        $songnow->track = $matches[1];
        $songnow->type  = $matches[2];
      }
    }
    if ( $this->status['state'] == 'play' ) {
      $songnow->started = $this->status['started'];
    }
    return $songnow;
  }

  /**
  * Query the vlc about it's current playlist and position
  */
  function GetQueue() {
    global $c;

    // $this->UpdateStatus();
    $this->queue = array();
    $this->playlist = array();

    $this->current = "";

    $response = $this->Daemon( "playlist" );
    $playlist = split( "\n", $response->text );
    foreach( $playlist AS $k => $v ) {
      if ( preg_match( '/^\| .*   (.*)\|([^\|]*)\|\s*$/', $v, $matches ) ) {
        $this->playlist[] = $matches[1];
        if ( $this->current != "" ) {
          $this->queue[] = $matches[1];
        }
      }
      elseif ( preg_match( '/^\|\*.*   (.*)\|([^\|]*)\|\s*$/', $v, $matches ) ) {
        $this->playlist[] = $matches[1];
        $this->current = $matches[1];
      }
    }

    return $this->queue;
  }

}

$GLOBALS["vlc"] = new vlcConnection();





/******************************************************************
* The actual API the web interface calls is then very simple...
******************************************************************/

/**
* Queue a file for playing
*/
function daemon_play_track( $path ) {
  global $vlc;
  error_log("adorno: DBG: Trying to play '$path'");
  $vlc->Play( $path );
}


/**
* Get a list of the files currently queued for the future
*/
function daemon_get_queue() {
  global $vlc;
  $q = $vlc->GetQueue();
  return $q;
}


/**
* Get the currently playing track and it's starting time
*/
function daemon_current_track() {
  global $vlc;
  return $vlc->GetCurrent();
}


/**
* Get the currently playing track and it's starting time
*/
function daemon_other_command( $action, $track ) {
  global $vlc;
  switch( $action ) {
    case 'resume':
      $vlc->Daemon('play');
      break;
    case 'pause':
      $vlc->Daemon($action);
      break;
    case 'next':
      $vlc->Daemon($action);
      break;
    default:
      error_log("adorno: WARNING: Unsupported command '$action'" );
      $vlc->Daemon($action);
  }

  return true;
}
