<?php

  if ( isset($_GET['act']) && $_GET['act'] == 'no' && ( !isset($_POST['act']) || $_POST['act'] == 'no') ) return;

  if ( isset($l) && isset($a) && isset($t) ) {
    $qry = new AwlQuery( "SELECT path_name, hash_key FROM tracks WHERE album = ? AND artist = ? AND title = ? ", $l, $a, $t );
    if ( $qry->Exec("PlayTracks") && $qry->rows() == 1 ) {
      $track = $qry->Fetch();
      PlayTrack( $track->path_name, $track->hash_key );
    }
  }
  else if ( ( isset($a) || (isset($play) && $play > 0) ) && isset($l) ) {
    // Play all of the tracks for this album
    if ( isset($a) )
      $qry = new AwlQuery( "SELECT path_name, hash_key FROM tracks WHERE album = ? AND artist = ? ORDER BY setpart, tracknum;", $l, $a );
    else
      $qry = new AwlQuery( "SELECT path_name, hash_key FROM tracks WHERE album = ? ORDER BY setpart, tracknum;", $l );
    if ( $qry->Exec("PlayTracks") && $qry->rows() > 0 ) {
      while( $track = $qry->Fetch() ) {
        PlayTrack( $track->path_name, $track->hash_key );
      }
    }
  }
  else if ( ( isset($_POST['url']) && $_POST['act'] == 'Play URL' )
          || ( isset($_GET['url']) && $_GET['act'] == 'Play URL' ) ) {
    $url = ( isset($_POST['url']) ? $_POST['url'] : $_GET['url'] );
    PlayTrack( $url, md5($url) );

    // Attempt to update the database with the stream details
    $sql = "SELECT 1 FROM streams WHERE stream_url = ?";
    $qry = new AwlQuery( $sql, $url );
    if ( $qry->rows() == 0 ) {
      $sql = "INSERT INTO streams ( stream_url ) VALUES( ? )";
      $qry = new AwlQuery( $sql, $url );
    }
  }
  else if ( isset($_GET['i']) && isset($_GET['d']) ) {
    daemon_move( intval($_GET['i']), (substr($_GET['d'],0,1) == 'd' ? 1 : -1) );
  }
