<?php
/**
* List artists
*/
echo "<h3>Artists</h3>\n<p>";
$where = '';
$params = array();
if ( isset($search) ) {
  $where = "(artist ~* :search OR album ~* :search OR title ~* :search OR path_name ~* :search) ";
  $params[':search'] = $search;
}
if ( isset($altr) ) {
  $altr = trim($altr);
  $where .= ($where == ''? '' : ' AND ');
  $where .= '(artist ~ :artist_match)';
  $params[':artist_match'] = '^'.$altr;
}
if ( $where != '' ) $where = 'WHERE '. $where;
$sql = 'SELECT distinct ON (lower(artist)) artist FROM tracks '.$where.' ORDER BY lower(artist)';
$qry = new AwlQuery( $sql, $params );

if ( $qry->Exec('artist') && $qry->rows() > 0 ) {
  while ( $artist = $qry->Fetch() ) {
    $display = htmlspecialchars($artist->artist);
    if ( trim($display) == "" ) $display = "&laquo;unknown&raquo;";
    echo " <a href=\"?type=artist&a=" . urlencode($artist->artist) . "$letter_get\" class=\"artist\">$display</a>\n";
  }
}

