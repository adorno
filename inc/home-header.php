<?php

  require_once("LinkClass.php");
  require_once("DataEntry.php");

function build_controls() {
  $url = '<a href="#" onclick="act(\'%s\');"><img src="/img/%s" title="%s" /></a>'."\n";
  $controls = "";

  $controls .= sprintf( $url, 'pause', 'pause.png', 'Pause the playback');
  $controls .= sprintf( $url, 'resume','resume.png','Resume the paused playback');
  $controls .= sprintf( $url, 'next',  'next.png',  'Skip ahead to the next track');
  $controls .= sprintf( $url, 'stop',  'stop.png',  'Stop playing tracks');
  $controls .= sprintf( $url, 'clear', 'clear.png', 'Clear the currently queued tracks');
  $controls .= sprintf( $url, 'off',   'off.png',   'Shut music server down and turn off');

  return $controls;
}



function build_menubar() {
  global $ltrtype, $a, $l, $lltr, $altr, $search;

  if ( !isset($ltrtype) ) $ltrtype = 'artist';
  $url = $_SERVER['PHP_SELF'] . build_url_params( array('altr' => '--altr--') );
  $ls = new LinkSet( '<a href="%s"%s%s>%s</a>', 'alphabetica' );

  $sf = new EntryForm( '', $GLOBALS, true );
  $sf->SimpleForm();
  $menubar = $sf->StartForm(array('method' => 'get', 'id' => 'search_form'));
  if ( isset($ltrtype) ) $menubar .= $sf->HiddenField( 'type', $ltrtype );
  if ( isset($lltr) )    $menubar .= $sf->HiddenField( 'lltr', $lltr );
  if ( isset($altr) )    $menubar .= $sf->HiddenField( 'altr', $altr );
  if ( isset($a) )       $menubar .= $sf->HiddenField( 'a', $a );
  else if ( isset($l) )  $menubar .= $sf->HiddenField( 'l', $l );

  $menubar .= '<table width="100%" border="0"><tr class="tr_menu"><td class="h3"><span id="current_type" onclick="toggle_type();">'.ucfirst($ltrtype).'</span>s:</td><td id="alphabet">';
  $qry = new AwlQuery("SELECT letter, pattern, $ltrtype"."_count AS count FROM letter_groups ORDER BY 1;");
  if ( $qry->Exec("artist") && $qry->rows() > 0 ) {
    while ( $letter = $qry->Fetch() ) {
      $help = "Display $letter->count $ltrtype"."s beginning with the letter '$letter->letter'.";
      $display = $letter->letter;
      if ( trim($letter->letter) == "#" ) {
        $help = 'List all '."$letter->count $ltrtype".'s not beginning with an alphabetic character.';
      }
      $menubar .= $ls->Link( str_replace('--altr--', rawurlencode($letter->pattern), $url), $display, $help );
    }
  }
  $menubar .= '</td>';

  $f = new EntryField( 'text', 'search',
                   array( 'title' => 'Enter a regular expression to search for artists / albums that match that',
                          'size'  => '15', 'class' => 'search_field'),
                   "$search" );
  $menubar .= '<td align="right">'.$f->Render().'</td>';
  $menubar .= '<td>'.$sf->SubmitButton("submit","Search").'</td>';

  $menubar .= '</tr></table>';

  $menubar .= $sf->EndForm();

  return $menubar;
}

$controls = build_controls();
$menubar  = build_menubar();
if ( !isset($stylesheet) ) $stylesheet = "adorno.css";

  echo <<<EOHTML
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Strict//EN" "http://www.w3.org/TR/html4/DTD/html4-strict.dtd">
<html>
<head>
<title>Adorno Music Server</title>
<link rel="stylesheet" type="text/css" href="$stylesheet" />
<link rel="shortcut icon" type="image/x-icon" href="/img/adorno.png">
<link rel="icon" type="image/x-icon" href="/img/adorno.png">
<script language="javascript" src="js/adorno.js" defer>Javascript is required, sorry.</script>
</head>
<body>
<table id="header">
 <tr>
  <td id="now_playing">Stopped</td>
  <td id="controls">
$controls  </td>
 </tr>
</table>
<div id="menu">
  $menubar
</div>
<div id="playurl">
<form id="urlform" method="POST"><input name="url" class="playurl" type="text"><input class="submit" type="submit" name="act" value="Play URL"></form>
</div>
<div id="content">

EOHTML;
