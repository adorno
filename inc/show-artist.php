<script language="javascript"><!--
function show_hide() {
  var args = show_hide.arguments;
  var tbody = document.getElementById('album-' + args[0]);
  var control = document.getElementById('control-' + args[0]);
//  alert(tbody.style.display);
  if ( tbody.style.display == 'none' ) {
    tbody.style.display = '';
    control.innerHTML = '–';
  }
  else {
    tbody.style.display = 'none';
    control.innerHTML = '+';
  }
}
--></script>
<?php
/**
* List artists
*/
function artist_track( $trk, $link_title, $row_class = "" ) {
  global $letter_get, $ltrtype;

  $safe_title = htmlspecialchars($link_title);
  $duration = preg_replace( "/^[0:]{1,4}/", "", $trk->duration );
  $enqueue = sprintf( "enqueue('%s','%s','%s')", rawurlencode($trk->artist),
                 rawurlencode($trk->album), rawurlencode($trk->title) );
  $link = <<<EOHTML
    <a class="track$row_class" onclick="$enqueue" title="$trk->path_name">$safe_title</a>
  </td>
  <td class="track$row_class" width="7%">$duration</td>
  <td class="track$row_class" width="4%">
    <a class="alphabetica" href="edit_track.php$track_link_url" title="Edit Track Info">E</a>

EOHTML;

  return $link;
}

if ( isset($a) ) {
  echo '<h3>'.htmlspecialchars($a).'</h3>';

  $qa = 'SELECT artist, album, title, tracknum, path_name, duration::interval(0), ';
  $qa .= 'extract( EPOCH FROM duration)::int AS secs, quality, get_last_played(hash_key) AS last_played ';
  $qa .= 'FROM tracks WHERE lower(artist) = lower(?) ';
  $qa .= 'ORDER BY lower(album), setpart, tracknum';
  $qry = new AwlQuery($qa, $a);
  if ( $qry->Exec() && $qry->rows() > 0 ) {
    echo '<table class="album_list" cellspacing="0">
';
    $last_album = '';
    $rownum = 0;
    $id = 0;
    while ( $track = $qry->Fetch() ) {
      if ( $track->album != '' && $track->album != $last_album ) {
        if ( $rownum != 0 ) echo '</tbody>';
        echo '<tr class="th4">';
        $id++;
        echo '<td width="3%" class="th4"><span class="expand_hide" id="control-'.$id.'" onclick="show_hide('.$id.')" title="Expand section">+</span></td>';
        echo '<td width="86%" class="th4" style="text-align:left"><a class="th4" onclick="';
        printf( "enqueue('%s','%s','')", rawurlencode($track->artist), rawurlencode($track->album) );
        echo '" title="Play Entire Album">'.htmlspecialchars($track->album).'</a></td>';
        echo '<td width="7%">&nbsp;</td>';
        echo '<td width="4%" class="th4"><a class="alphabetica"  class="track" href="edit_track.php?l='
 . rawurlencode($track->album) . '&a='
 . rawurlencode($track->artist) . '" title="Edit Album Info">E</a>
</td></tr>
<tbody id="album-'.$id.'" style="display:none;">
<tr class="th4">
';
        $last_album = $track->album;
      }
      else if ( $rownum == 0 ) {
        echo '<tr class="th4">';
        $id++;
        echo '<td width="3%" class="th4"><span class="expand_hide" id="control-'.$id.'" onclick="show_hide('.$id.')" title="Expand section">+</span></td>';
        echo '<td class="th4" colspan="3" style="text-align:left">Unkown Album</td></tr>
<tbody id="album-'.$id.'" style="display:none;">
<tr class="th4">
';
      }
      else {
        echo '<tr class="th4">';
      }
      $rowclass = ($rownum % 2);
      echo '<td colspan="2" style="text-align:left;" class="track'.$rowclass.'">';
      echo artist_track($track, ($track->tracknum > 0 ? $track->tracknum.': ' : '') . $track->title,  $rowclass );
      $rownum++;
      echo '</td>';
      echo '</tr>
';
    }
    if ( $rownum != 0 ) echo '</tbody>';
    echo '</table>';
  }

}




