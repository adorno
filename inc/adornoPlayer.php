<?php

error_log( "Using Adorno backend: ". $c->daemon_type );
function daemon_play_track( $path ) {
  $fifo = fopen( "/var/run/adorno/fifo", "w" );
  fputs( $fifo, "queue $path\n" );
  fclose($fifo);

  return true;
}

function daemon_current_track() {
  $result = (object) array();

  if ( ! file_exists( "/var/run/adorno/queue.txt" ) ) return $result;

  $fd = fopen( "/var/run/adorno/queue.txt", "r" );
  if ( $fd ) {
    $track = fgets( $fd, 300 );
    $track = trim( $track, "\n" );
    $started = fgets( $fd, 300 );
    $started = trim( $started, "\n" );
    fclose($fd);

    $result->track = $track;
    $result->started = $started;
  }

  return $result;
}

function daemon_get_queue() {
  if ( ! file_exists( "/var/run/adorno/queue.txt" ) ) return array();
  $fd = fopen( "/var/run/adorno/queue.txt", "r" );
  if ( $fd ) {
    $track = fgets( $fd, 2048 );  // Skip the "now playing" - it showed at the top.
    $track = fgets( $fd, 300 );  // Skip the start time...

    $tracks = array();
    while( !feof( $fd ) ) {
      $tracks[] = fgets( $fd, 2048 );  // Yes: we assume all tracks are less than 2048 bytes.
    }
    fclose($fd);
  }
  return $tracks;
}

function daemon_other_command( $action, $track ) {
  $fifo = fopen( "/var/run/adorno/fifo", "w" );
  if ( ! preg_match( '{^(/music)|(http://)}i', $track ) ) {
    $track = "/music" . "$track";
  }
  # $track = str_replace( " ", "\\ ", $track);
  fputs( $fifo, "$action $track\n" );
  fclose($fifo);
}
