<?php

error_log( "Using MPD backend: ". $c->daemon_type );
class mpdConnection {
  var $active;            // Once it actually is...
  var $mpd;
  var $status;

  /**
  * Create a minimally initialised connection object
  */
  function mpdConnection() {
    $this->active = false;
    $this->mpd = false;
    $this->status = array();
  }

  /**
  * Connect to the remote mpd
  */
  function Connect() {
    global $c;

    $this->mpd = stream_socket_client($c->mpd['connection'], $errno, $errstr, 2);
    $result = fgets($this->mpd, 8192);
    $this->active = true;
  }

  /**
  * Do a question/response pair with the daemon
  */
  function Daemon( $say ) {
    if ( ! $this->active ) $this->Connect();

    $say .= "\n";
    fwrite($this->mpd, $say );

    $result = "";
    while ( ! feof($this->mpd) ) {
      $line = fgets($this->mpd, 8192);
      if ( strncmp($line, 'OK', 2) == "0" ) break;
      $result .= $line;
      if ( strncmp($line, 'ACK', 3) == "0" ) break;
    }

    return $result;
  }

  /**
  * Query the MPD about it's current status
  */
  function UpdateStatus() {
    $status = split( "\n", $this->Daemon( "status" ) );
    foreach( $status AS $k => $v ) {
      list( $key, $value ) = preg_split( '/:\s*/', $v, 2);
      $this->status[$key] = $value;
    }
  }


  /**
  * Play a track.  If the current status is 'stop' then we
  * first clear the playlist and after tell mpd to play.
  */
  function Play( $track ) {
    global $c;

    $this->UpdateStatus();
    $stopped = ( $this->status['state'] == 'stop' );

    $track = str_replace( $c->mpd['prefix'], '', $track);

    $result = $this->Daemon("add $track");
    if ( preg_match( '/ACK.*not found/', $result, $matches ) ) {
      $result = $this->Daemon("update $track");
      sleep(1);
      for( $counter=0; $this->UpdateStatus() && isset($this->status['updating_db']) && $counter++ < 10; sleep(1) );
      $result = $this->Daemon("add $track");
    }
    if ( $stopped ) $this->Daemon( sprintf('play %d', $this->status['playlistlength']) );
  }


  /**
  * Query the MPD about it's current playlist and position
  */
  function GetCurrent() {
    global $c;

    $this->UpdateStatus();
    $songnow = (object) array();
    if ( $this->status['state'] == 'stop' ) return $songnow;

    $current = split( "\n", $this->Daemon( "currentsong" ) );
    foreach( $current AS $k => $v ) {
      list( $key, $value ) = preg_split( '/:\s*/', $v, 2);
      if ( $key == 'file' ) {
        $songnow->track = $c->mpd['prefix'] . $value;
        break;
      }
    }
    if ( $this->status['state'] == 'play' ) {
      $seconds_in = preg_replace( '/:.*$/', '', $this->status['time']);
      $songnow->started = date( 'Y-m-d H:i:s', time() - $seconds_in );
    }
    return $songnow;
  }

  /**
  * Query the MPD about it's current playlist and position
  */
  function GetQueue() {
    global $c;

    $this->UpdateStatus();
    $next = $this->status['song'] + 1;
    $this->queue = array();

    $filename = "";

    $playlist = split( "\n", $this->Daemon( "playlistinfo" ) );
    foreach( $playlist AS $k => $v ) {
      list( $key, $value ) = preg_split( '/:\s*/', $v, 2);

      switch( $key ) {
        case 'file':
          $filename = $c->mpd['prefix'] . $value;
          break;
        case 'Pos':
          $pos = $value - $next;
          if ( $pos >= 0 ) {
            $this->queue[$pos] = $filename;
          }
          break;
      }
    }

    return $this->queue;
  }

}

$GLOBALS["mpd"] = new mpdConnection();





/******************************************************************
* The actual API the web interface calls is then very simple...
******************************************************************/

/**
* Queue a file for playing
*/
function daemon_play_track( $path ) {
  global $mpd;
  error_log("adorno: DBG: Trying to play '$path'");
  $mpd->Play( $path );
}


/**
* Get a list of the files currently queued for the future
*/
function daemon_get_queue() {
  global $mpd;
  $q = $mpd->GetQueue();
  return $q;
}


/**
* Get the currently playing track and it's starting time
*/
function daemon_current_track() {
  global $mpd;
  return $mpd->GetCurrent();
}


/**
* Get the currently playing track and it's starting time
*/
function daemon_other_command( $action, $track ) {
  global $mpd;
  switch( $action ) {
    case 'resume':
      $mpd->Daemon('play');
      break;
    case 'pause':
      $mpd->Daemon($action);
      break;
    case 'next':
      $mpd->Daemon($action);
      break;
    default:
      error_log("adorno: WARNING: Unsupported command '$action'" );
      $mpd->Daemon($action);
  }

  return true;
}
