<?php
  require_once("LinkClass.php");
  require_once("DataEntry.php");

  $url = '<a href="#" onclick="act(\'%s\');"><img src="/img/%s" title="%s" /></a>'."\n";
  $controls = "";

  $controls .= sprintf( $url, 'pause', 'pause.png', 'Pause the playback');
  $controls .= sprintf( $url, 'resume','resume.png','Resume the paused playback');
  $controls .= sprintf( $url, 'next',  'next.png',  'Skip ahead to the next track');
  $controls .= sprintf( $url, 'stop',  'stop.png',  'Stop playing tracks');
  $controls .= sprintf( $url, 'clear', 'clear.png', 'Clear the currently queued tracks');
  $controls .= sprintf( $url, 'off',   'off.png',   'Shut music server down and turn off');

  $url = "";
  if ( "$lltr" <> "" )     $url .= "&lltr=" . rawurlencode($lltr);
  elseif ( "$altr" <> "" ) $url .= "&altr=" . rawurlencode($altr);
  if ( "$a" <> "" )      $url .= "&a=" . rawurlencode($a);
  elseif ( "$l" <> "" )  $url .= "&l=" . rawurlencode($l);

  if ( !isset($ltrtype) ) {
    $ltrtype = 'artist';
    if ( preg_match( '/album.php/', $_SERVER['REQUEST_URI']) ) $ltrtype = 'album';
  }

  $url = $_SERVER['PHP_SELF'] . preg_replace( '/^&/', '?', $url . '&' );
  if ( !preg_match( '{/(artist|album)\.php}', $url) ) {
    error_log( "$sysabbr: DBG: >>$url<< " . (preg_match('{/(artist|album)\.php}', $url)? 'true':'false') );
    // $url = preg_replace( '{/[a-z0-9_-]+\.php}i', '/artist.php', $url );
    $url .= sprintf('type=%s&', $ltrtype);
  }
  $ls = new LinkSet( '<a href="'.str_replace('%','%%',$url).'altr=%s"%s%s>%s</a>', 'alphabetica' );
  error_log( "$sysabbr: DBG: >>$ls->template<<" );

  $menubar = '<table width="100%" border="0"><tr class="tr_menu"><td class="h3"><span id="current_type" onclick="toggle_type();">'.ucfirst($ltrtype).'</span>s:</td><td id="alphabet">';
  $qry = new AwlQuery("SELECT letter, pattern, $ltrtype"."_count AS count FROM letter_groups ORDER BY 1;");
  if ( $qry->Exec("artist") && $qry->rows() > 0 ) {
    while ( $letter = $qry->Fetch() ) {
      $help = "Display $letter->count $ltrtype"."s beginning with the letter '$letter->letter'.";
      $display = $letter->letter;
      if ( trim($letter->letter) == "#" ) {
        $help = 'List all '."$letter->count $ltrtype".'s not beginning with an alphabetic character.';
      }
      $menubar .= $ls->Link( urlencode($letter->pattern), $display, $help );
    }
  }
  $menubar .= '</td>';
//  $menubar .= '<td class="h3">Search:</td>';
  $sf = new EntryForm( $refresh_url, $GLOBALS, true );
  $sf->SimpleForm();
  $menubar .= $sf->StartForm(array('method' => 'get'));
  if ( isset($lltr) )   $menubar .= $sf->HiddenField( 'lltr', $lltr );
  if ( isset($altr) )   $menubar .= $sf->HiddenField( 'altr', $altr );
  if ( isset($a) )      $menubar .= $sf->HiddenField( 'a', $a );
  else if ( isset($l) ) $menubar .= $sf->HiddenField( 'l', $l );
  $f = new EntryField( 'text', 'search',
                   array( 'title' => 'Enter a regular expression to search for artists / albums that match that',
                          'size'  => '15', 'class' => 'search_field'),
                   "$search" );
  $menubar .= '<td align="right">'.$f->Render().'</td>';
  $menubar .= '<td>'.$sf->SubmitButton("submit","Search").'</td>';
  $menubar .= $sf->EndForm();

  $ls = new LinkSet("", "alphabetica");
  $menubar .= '<td align="right">';
  $menubar .= $ls->Link("/", "Home");
  $menubar .= $ls->Link("/artist.php", "Artist");
  $menubar .= $ls->Link("/album.php", "Album");
//  $menubar .= $ls->Link("/genre.php", "Genre");
//  $menubar .= $ls->Link("/stream.php", "Streams");
  $menubar .= '</td>';

  $menubar .= '</tr></table>';

//  $menubar .= '<hr class="thin_hr">';

  echo <<<EOHTML
<body>
<script language="javascript"><!--
function toggle_type() {
  var type_span = document.getElementById('current_type');
  var type = type_span.innerHTML.toLowerCase();
  var old_type = type;
  // alert(type);
  if ( type == 'artist' )       { type = 'album'; }
  else if ( type == 'album' )   { type = 'stream'; }
  else if ( type == 'stream' )  { type = 'genre'; }
  else if ( type == 'genre' )   { type = 'artist'; }
  // alert(type);
  type_span.innerHTML = type.substring(0,1).toUpperCase() + type.substring(1);

  var alphabet = document.getElementById('alphabet').childNodes;
  
  var href;
  var title;
  var title_match = new RegExp('([0-9]+ )?' + old_type,'g');
 
  for (var i = 0; i < alphabet.length; i++) 
  {
    href = alphabet[i].href;
    alphabet[i].href = href.replace(old_type,type);
    title = alphabet[i].title;
    alphabet[i].title = title.replace(title_match,type);
//    if ( alphabet[i].innerHTML == 'F' ) {
//      alert(alphabet[i].title);
//    }
  };

}

var base_url = 'http://aiw/';
var status_req = new XMLHttpRequest();
var mytime;
var redo_queue = true;
function update_now_playing() {
  var td = document.getElementById('now_playing');
  status_req.open('GET', base_url + 'current.php', true);
  status_req.onreadystatechange = function (aEvt) {
    if ( status_req.readyState == 4 ) {
      if ( status_req.status == 200 ) {
        if ( td.innerHTML != status_req.responseText ) {
          td.innerHTML = status_req.responseText;
          var in_td = td.childNodes;
          var title = '';
          if ( in_td.length == 1 ) {
            var nodecontent = status_req.responseText;
            if ( nodecontent.match(/Nothing is currently playing/i) ) {
              title = 'Adorno: stopped';
            }
            else if ( nodecontent.match(/paused/i) ) {
              title = 'Adorno: paused';
            }
            in_td = in_td[0].childNodes;
          }
          for ( var i=0; i < in_td.length; i++ ) {
            if ( in_td[i].innerHTML == undefined ) continue;
            if ( title == '' )
              title = in_td[i].innerHTML;
            else
              title = title + ' : ' + in_td[i].innerHTML;
          }
          document.title = title;
          update_queue();
        }
        else if ( redo_queue )
          update_queue();
      }
//      else
//        td.innerHTML = "<p>Error loading page</p>";
    }
  };
  status_req.send(null);
  mytime=setTimeout('update_now_playing()',10000);
}

function update_queue() {
  var td = document.getElementById('queue');
  redo_queue = false;
  status_req.open('GET', base_url + 'queue.php', true);
  status_req.onreadystatechange = function (aEvt) {
    if ( status_req.readyState == 4 ) {
      if ( status_req.status == 200 ) {
        td.innerHTML = status_req.responseText;
      }
//      else
//        td.innerHTML = "<p>Error loading page</p>";
    }
  };
  status_req.send(null);
}

var last_action_result = '';
function act( action ) {
  var act_req = new XMLHttpRequest();
  status_req.open('GET', base_url + 'action.php?action=' + action, true);
  status_req.onreadystatechange = function (aEvt) {
    if ( status_req.readyState == 4 ) {
      if ( status_req.status == 200 ) {
        last_action_result = 'success';
        redo_queue = true;
        update_now_playing();
      }
      else
        last_action_result = 'failure';
    }
  };
  status_req.send(null);
}

function enqueue(artist,album,track) {
  var act_req = new XMLHttpRequest();
  var details = '&a=' + artist + '&l=' + album;
  if ( track != '' ) details = details + '&t=' + track;
  var url = base_url +  'action.php?action=enqueue' + details
  // alert(url);
  status_req.open('GET', url, true);
  status_req.onreadystatechange = function (aEvt) {
    if ( status_req.readyState == 4 ) {
      if ( status_req.status == 200 ) {
        last_action_result = 'success';
        redo_queue = true;
        update_now_playing();
      }
      else
        last_action_result = 'failure';
    }
  };
  status_req.send(null);
}
--></script>
<table id="header">
 <tr>
  <td id="now_playing">Stopped</td>
  <td id="controls">
$controls  </td>
 </tr>
</table>
<div id="menu">
  $menubar
</div>
<div id="playurl">
<form id="urlform" method="POST"><input name="url" class="playurl" type="text"><input class="submit" type="submit" name="act" value="Play URL"></form>
</div>
<script language="javascript"><!--
update_now_playing();
--></script>
<div id="content">

EOHTML;
