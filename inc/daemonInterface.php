<?php

include_once('AwlQuery.php');

switch ( $c->daemon_type ) {
  case 'mpd':    include("mpdPlayer.php");      break;
  case 'vlc':    include("vlcPlayer.php");      break;
  case 'xmms2':  include("xmms2Player.php");    break;
  default:       include("adornoPlayer.php");
}

/**
* High level function to play a track
*/
function PlayTrack( $path, $hash ) {
  if ( isset($_GET['act']) && $_GET['act'] == 'no' && ( !isset($_POST['act']) || $_POST['act'] == 'no') ) {
    error_log( sprintf("DBG: PlayTrack: *NOT* Queueing '%s' for playing", $path) );
    return;
  }

  error_log( sprintf("DBG: PlayTrack: Queueing '%s' for playing", $path) );

  if ( daemon_play_track($path) ) {
    $qry = new AwlQuery( "SELECT track_played(?) ", $hash );
    $qry->Exec("PlayTrack");
  }
}


/**
* High level function to get the current track as a database object
*/
function current_track() {

  $current_track = daemon_current_track();

  $query = "SELECT *, extract( EPOCH FROM duration)::int AS secs ";
  if ( preg_match( '/[0-9]{4}-[0-9]{2}-[0-9]{2} [0-9]{2}:[0-9]{2}/', $current_track->started ) ) {
    $query .= ", extract( EPOCH FROM ('".$current_track->started."'::timestamp + duration))::int AS finishing ";
  }
  $query .= "FROM tracks WHERE path_name = ? ; ";

  $qry = new AwlQuery( $query, $current_track->track );
  if ( $qry->Exec('current_track') && $qry->rows() > 0 ) {
    $track = $qry->Fetch();
    $track->state = $current_track->state;
    return $track;
  }
  return;
}


/**
* High level function to crudely display the current track
*/
function show_queue() {

  $queue = daemon_get_queue();

  echo "<h3>Coming up...</h3>\n<p class=\"track_queue\">";
  foreach( $queue AS $k => $v ) {
    echo nice_track_name($v) . "<br>\n";
  }
  echo "</p>";
}


/**
* High level function to return an array of $track objects
*/
function current_queue() {

  $queue = daemon_get_queue();

  $position = 0;
  $in_list = '';
  $params = array();
  foreach( $queue AS $k => $track ) {
    $param_id = ':t'.$position;
    $in_list .= ($position++ == 0 ? '' : ', ') . $param_id ;
    $params[$param_id] = $track;
  }

  if ( $in_list == '' ) return $queue;

  /**
  * Select the track information from the database
  */
  $tinfo = array();
  $sql = sprintf("SELECT *, EXTRACT( 'epoch' FROM duration ) AS dur_secs FROM tracks WHERE path_name IN ( %s );", $in_list );
  // printf( "\n%s\n", $in_list );
  // print_r($params);
  $qry = new AwlQuery( $sql, $params );
  if ( $qry->Exec("current_queue",__LINE__,__FILE__) && $qry->rows() ) {
    while( $track = $qry->Fetch() ) {
      $tinfo[$track->path_name] = $track;
    }
  }
  // print_r( $tinfo );

  foreach( $queue AS $k => $track ) {
    // printf( '<p>%s</p>', $track );
    $queue[$k] = $tinfo[$track];
  }

  // Just make sure that's sorted in key order now...
  ksort($queue);

  return $queue;
}


