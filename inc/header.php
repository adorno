<?php

  /////////////////////////////////////////////////////////
  // Work out our page title
  /////////////////////////////////////////////////////////
  if ( !isset($page_title) || $page_title == "" ) {
    $current_track = current_track();
    if ( is_object($current_track) ) {
      $page_title = "$current_track->title : $current_track->album : $current_track->artist  ";
      $refresh_time = ( $current_track->finishing - time() ) + 5;
    }
  }

  if ( isset($action) ) daemon_other_command($action,$track);


  if ( !isset($stylesheet) ) $stylesheet = "adorno.css";
  echo <<<DOCHEADER
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.0 Strict//EN" "http://www.w3.org/TR/html4/DTD/html4-strict.dtd">
<html>
<head>
<title>$page_title</title>
<link rel="stylesheet" type="text/css" href="$stylesheet" />
</head>

DOCHEADER;

include("menu.php");
