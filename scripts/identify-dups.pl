#!/usr/bin/perl -w

# This script takes as an input the output of a command like
#
# find /data -type f -exec md5sum {} \;
#
# and generates a script to remove duplicates, optionally creating symlinks.

use Digest::MD5;

use MP3::Info;
use Ogg::Vorbis::Header;

use String::ShellQuote;

$totalFiles = 0;
$totalLinked = 0;
$totalRemoved = 0;

#The master patterns are a prioritised list that indicates where the master copy of a file should be kept.

@findMasterPatterns = ();
push @findMasterPatterns, '^/data/public/Music/Production/Kevin/';
push @findMasterPatterns, '^/data/public/Music/Production/Andrew/';
push @findMasterPatterns, '^/data/public/Music/Production/Grouped/';
push @findMasterPatterns, '^/data/public/Photos/Structured/';
push @findMasterPatterns, '^/data/public/Christopher/';
push @findMasterPatterns, '^/data/public/Music/nietzche/';
push @findMasterPatterns, '^/data/public/Music/Production/Karl/';
push @findMasterPatterns, '^/data/public/Photos/Lost-Dates/';
push @findMasterPatterns, '^/data/public/Music/Danny_Todd/';
push @findMasterPatterns, '^/data/public/Music/Production/Daniel/';
push @findMasterPatterns, '^/data/public/Music/Production/Simon/Pop/';
push @findMasterPatterns, '^/data/public/movies/TV Episodes/';
push @findMasterPatterns, '^/data/public/Music/Production/01 Singles .Graydon./';
push @findMasterPatterns, '^/data/public/Music/Production/01 Singles .Peter./';
push @findMasterPatterns, '^/data/public/Erica/fromwork/Thesis/Histories of the discipline/';
push @findMasterPatterns, '^/data/public/Erica/fromwork/Thesis/Historis of the University in NZ/';
#push @findMasterPatterns, '';
#push @findMasterPatterns, '';

#The link patterns are a specification of which directories should have links generated - i.e. the original file
#names might be referenced.  . should always create links.

@linkPatterns = ();
push @linkPatterns, '^/data/public/Music/Production/01 Singles .Erica./';
push @linkPatterns, '^/data/public/Christopher/';

#print "@findMasterPatterns findMasterPatterns loaded.\n";
#print "@linkPatterns linkPatterns loaded.\n";

open(SCRIPT,">  script.sh") || die("can't open script.sh: $!");
print SCRIPT "#!/bin/sh\n";
open(MD5s,'md5sorted.txt') || die("can't open md5sorted.txt: $!");

my $numCount = 0;
my @tmp = (0,0);
my $lastMD5 = '';
my $newMD5 = 'xxx';
my $fileName = undef;
my $leftOver = undef;
my @dataStore = undef;
my $firstOne = 1;

sub loadData
{
  $numCount++;
  $dataStore[$numCount]="/" . $fileName;
  if ($leftOver) { print "UNEXPECTED SPLIT VALUE FOR HASH $newMD5 RESIDUAL $leftOver\n" }
}

sub resetStore
{
  $totalFiles += $numCount;
  $numCount = 0;
  @dataStore = undef;
  $dataStore[0] = $newMD5;
  loadData();
}

while ( <MD5s> ) {
  $lastMD5 = $newMD5;
  chomp;
  ($newMD5,$fileName,$leftOver) = split m!  /!;
  if ($lastMD5 eq $newMD5 || $firstOne)
  {
    #just load into the main data hash
    $firstOne = 0;
    loadData();
  }
  elsif ($numCount == 1)
  {
    #It's different, but there was only one of them
    #Because we're processing duplicates, no action needed
    print SCRIPT "#IGNORE $dataStore[1]\n";
    resetStore();
  }
  else {
    #actually do the work - this is split out as the input file will end, and we still want to process in that case
    #note the last action is still to read resetStore
    nextAndLast();
  }
}

if ($numCount > 1) { nextAndLast(); }

print "In this number of files:       $totalFiles\n";
print "Linked:                        $totalLinked\n";
print "Removed:                       $totalRemoved\n";
if ($totalFiles) {
printf "Tidied percentage              %2.1f\n", ($totalLinked + $totalRemoved) / $totalFiles * 100;
}

sub nextAndLast()
{
  #process the records associated with the previous md5sum, which are in @dataStore[1 to $numCount]

  #find the one to keep

  $oneToKeep = 0;
  $patternID = 0;

  until ($oneToKeep or $patternID == @findMasterPatterns) {
    $patternID++;
    $i = 0;
    until ($oneToKeep or $i == $numCount) {
      $i++;
      if ($dataStore[$i] =~ $findMasterPatterns[$patternID-1]) {$oneToKeep = $i;} ;
    }
  }

  if (@dataStore) { #if not, then first run
    unless ($oneToKeep) {
      die "Could not find one to keep for @dataStore"
    }
  };

  #symlink or delete the others based on directory

  for ($i=1;$i<$numCount+1;$i++)
  {
    if ($i == $oneToKeep) #if this is the one to keep
    {
      print SCRIPT "#RETAIN $dataStore[$i]\n";
    }
    else
    {
      $done = 0;
      $j = 0;
      until ($done or $j==@linkPatterns)
      {
        $j++;
        if ($dataStore[$i] =~ $linkPatterns[$j-1]) {
          @temp = shell_quote ("rm", "$dataStore[$i]");
          @temp2 = shell_quote ("ln", "-s", "$dataStore[$oneToKeep]", "$dataStore[$i]") ;
          print SCRIPT "@temp && @temp2\n";
          $done = 1;
          $totalLinked++;
        }
      }

      unless ($done) {
        @temp = shell_quote ("rm", "$dataStore[$i]");
        print SCRIPT "@temp\n";
        $totalRemoved++;
      }
    }
  }

  # And reset the data store etc.
  resetStore();

}
