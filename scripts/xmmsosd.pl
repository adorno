#!/usr/bin/perl -w

# lcdxmms.pl - an example client for LCDproc

#
# Simple client for LCDproc which displays current status from xmms2
#

use strict;
use Getopt::Long;
use IO::Socket;
use Fcntl;

my $debug = 0;

# Host which runs lcdproc daemon (LCDd)
my $SERVER = "localhost";

# Port on which LCDd listens to requests
my $PORT = "13666";

# XMMS status command
my @xmms = qw{xmms2 status};

# Gracefully exit, since we won't otherwise
$SIG{INT} = \&graceful_close;
$SIG{TERM} = \&graceful_close;

# Connect to the LCD Daemon...
my $lcdd = IO::Socket::INET->new(
		Proto     => 'tcp',
		PeerAddr  => $SERVER,
		PeerPort  => $PORT,
	)
	|| die "Cannot connect to LCDproc port\n";

# Make sure our messages get there right away
$lcdd->autoflush(1);

sleep 1;	# Give server plenty of time to notice us...

print $lcdd "hello\n";
my $lcdresponse = <$lcdd>;
print $lcdresponse if ( $debug );

# determine LCD size (not needed here, but useful for other clients)
($lcdresponse =~ /lcd.+wid\s+(\d+)\s+hgt\s+(\d+)/);
my $lcdwidth = $1; my $lcdheight= $2;
print "Detected LCD size of $lcdwidth x $lcdheight\n" if ($debug);

# Turn off blocking mode...
fcntl($lcdd, F_SETFL, O_NONBLOCK);

# Set up some screen widgets...
print $lcdd "client_set name {XMMS}\n";
$lcdresponse = <$lcdd>;
print $lcdd "screen_add xmmsosd\n";
$lcdresponse = <$lcdd>;
print $lcdd "screen_set xmmsosd -name {Adorno} -heartbeat off -wid $lcdwidth -hgt $lcdheight\n";
$lcdresponse = <$lcdd>;
print $lcdd "widget_add xmmsosd title string\n";
$lcdresponse = <$lcdd>;
print $lcdd "widget_set xmmsosd title 1 1 {-+ * Adorno * +-}\n";
$lcdresponse = <$lcdd>;
print $lcdd "widget_add xmmsosd track scroller\n";
$lcdresponse = <$lcdd>;


while(1) {
  my $status = `xmms2 status`;
  $status =~ s{^\s*(\S.*\S)\s*$}{$1};
  print STDERR 'status: ', $status, "  xxxx\n" if ( $debug );
  write_lcd_text($lcdd,$status);
  while (defined(my $line = <$lcdd>)) {
    chomp $line;
#    print "Received '$line'\n" if ($debug);
    my @items = split(/ /, $line);
    my $command = shift @items;
  }

  # wait a bit
  sleep 5;
}


sub write_lcd_text {
  my $lcdd = shift;
  my $text = shift;

  # replace all whitespace by single spaces
  $text =~ s{\s+}{ }gs;
  $text =~ s{^\s*(\S.*\S)\s*$}{$1};
  my $status;
  $text =~ s{^(\S+):\s*}{} && do {
    $status = $1;
  };
  $text =~ s{: \s* \d{2}:\d{2}  \s* of \s* \d{2}:\d{2}\s*}{}x;

  if ( $status eq 'Stopped' ) {
    print $lcdd "screen_set xmmsosd -backlight off\n";
    $text = '';
  }
  else {
    print $lcdd "screen_set xmmsosd -backlight on\n";
#    my $response = <$lcdd>;
#    print 'response: ', $response, "\n" if ( $debug );
  }

  my $now = scalar localtime;
  $now =~ s{^.*\s(\d{2}:\d{2}):\d{2}\s.*$}{$1};
  print $lcdd "widget_set xmmsosd title  1 1 {$now : $status}\n";
  print $lcdd "widget_set xmmsosd track 1 2 16 2 h 3 {  $text  }\n";
  $text = <$lcdd>;

  return $text;  # not that we expect to use it, but hey...
}


sub graceful_close {
  print "Exiting...\n" if ($debug);
  close($lcdd);
  exit;
}


sub error {
  my $status = shift;
  my @msg = @_;

  die join(" ", @msg) . "\n";

  exit($status)  if ($status);
}


sub usage {
  my $status = shift;

  print STDERR "Usage: lcdxmms [<options>]\n";
  if (!$status) {
    print STDERR <<EOUSAGE;
  where <options> are\n" .
    -s <server>     connect to <server> (default: $SERVER)
    -p <port>       connect to <port> on <server> (default: $PORT)
    --debug         enable debugging messages
      show this help page

EOUSAGE
  }
  else {
    print STDERR "For help, type: lcdxmms --help\n";
  }  

  exit($status);
}

