#!/usr/bin/perl

use strict;
use warnings;
use Carp;

use Cwd qw(cwd abs_path);
use Digest::MD5;
use DBI;

use MP3::Info;
use Ogg::Vorbis::Header;

my %opts = (
             'database' => 'adorno',
             'dbuser'   => 'adorno_app',
             'force'    => 0,
             'remove'   => 0,
             'clean'    => 0,
             'update'   => 0,
             'quiet'    => 0,
             'sleep'    => 20,
             'duration' => 0,
             'debug'    => 0 );

my @file_list;
my $processed_count = 0;  # Count the files we actually process.

sub usage {
  print <<EOUSAGE ;
Usage:
  $0 [options ...] [path ...]

Options:
  -d | --database <dbname>  Log to this database (default: "adorno")
  -u | --user <username>    Log to database as this user (default: $ENV{'USER'})
  -s | --sleep <secs>       Sleep for a second after adding this many files (default: 20)
  -f | --force    Index all files we find. (default: only index new files)
  -q | --quiet    Be quiet - don't output progress messages.
  -r | --remove   Remove all equal hash files that are not the file we are processing
  -c | --clean    Remove all equal hash files that are not the file we are processing
  --debug     Debugging information
  --help      Request this help text

EOUSAGE

#  --update    Update the hash (and all other information) on any pathname match (TODO)
#  --duration  Only update duration for existing tracks (still add new ones though) (TODO)

  exit shift;
}


while( my $opt = shift ) {
  if ( $opt =~ /^-/ ) {
    if    ( $opt =~ /^-(d|-database)$/ ) { $opts{'database'}  = shift; }
    elsif ( $opt =~ /^-(u|-user)$/     ) { $opts{'dbuser'}    = shift; }
    elsif ( $opt =~ /^-(s|-sleep)$/    ) { $opts{'sleep'}     = shift; }
    elsif ( $opt =~ /^-(f|-force)$/    ) { $opts{'force'}     = 1;     }
    elsif ( $opt =~ /^-(q|-quiet)$/    ) { $opts{'quiet'}     = 1;     }
    elsif ( $opt =~ /^-(r|-remove)$/   ) { $opts{'remove'}    = 1;     }
    elsif ( $opt =~ /^-(c|-clean)$/    ) { $opts{'clean'}     = 1;     }
    elsif ( $opt =~ /^--update$/       ) { $opts{'update'}    = 1;     }
    elsif ( $opt =~ /^--duration$/     ) { $opts{'duration'}  = 1;     }
    elsif ( $opt =~ /^--debug$/        ) { $opts{'debug'}     = 1;     }
    elsif ( $opt =~ /^--help$/         ) { usage(0); }
    else {
      usage(1);
    }
  }
  else {
    push @file_list, $opt ;
  }
}


my $dbh = DBI->connect("dbi:Pg:dbname=$opts{'database'}", $opts{'dbuser'} ) or die "Can't connect to database $opts{'database'}";

#################################################################
# Prepare queries
#################################################################
my $insert_track = $dbh->prepare( <<EOQ  ) or die $dbh->errstr;
INSERT INTO tracks
( hash_key, path_name, title, artist, album, tracknum, setpart, duration, quality )
VALUES(  ?,         ?,     ?,      ?,     ?,        ?,       ?,  ?::interval,   ? )
EOQ

my $clean_tracks_by_path = $dbh->prepare( <<EOQ  ) or die $dbh->errstr;
DELETE FROM tracks WHERE path_name LIKE ?
EOQ

my $delete_track_by_path = $dbh->prepare( <<EOQ  ) or die $dbh->errstr;
DELETE FROM tracks WHERE path_name = ?
EOQ

my $select_hash_by_path = $dbh->prepare( <<EOQ  ) or die $dbh->errstr;
SELECT hash_key FROM tracks WHERE path_name = ?
EOQ

my $delete_track_by_hash = $dbh->prepare( <<EOQ  ) or die $dbh->errstr;
DELETE FROM tracks WHERE hash_key = ?
EOQ





#################################################################
# Deal to a single file.
#################################################################
sub one_file {
  my ( $filename ) = @_;

  return if ( $filename !~ /\.(ogg|mp3)$/ );

  print "." unless( $opts{'quiet'} );
  my $safe_path = $filename;

  if ( $opts{'force'} ) {
    $delete_track_by_path->execute( $safe_path ) or die $dbh->errstr;
  }
  else {
    $select_hash_by_path->execute( $safe_path ) or die $dbh->errstr;
    if ( $select_hash_by_path->rows > 0 ) {
      $select_hash_by_path->finish();
      return;
    }
  }
  my @fstats = stat($filename);

  open(FILE, $filename) or die "Can't open '$filename': $!";
  binmode(FILE);
  my $hashkey = Digest::MD5->new->addfile(*FILE)->hexdigest;
  close(FILE);
  print "#" unless( $opts{'quiet'} );

  $processed_count++;
  sleep(1) if ( $opts{'sleep'} > 0 && $processed_count % $opts{'sleep'} == 0 );

  if ( $opts{'remove'} ) {
    $delete_track_by_hash->execute( $hashkey ) or die $dbh->errstr;
  }

  my $tartist = "Unknown";
  my $talbum  = "Unknown";
  my $ttitle  = "Unknown";
  my $tyear   = "1066";
  my $tnum    = 0;
  my $setpart = 1;
  my $tlength  = 0;
  my $tquality = "HUH: Appears dodgy!";

  if ( $filename =~ /\.(ogg|mp3)$/ ) {

    if ( $filename =~ /\.mp3$/ ) {
      # Get the info out of the (hopefully) .mp3 file
      my $mp3info = get_mp3info($filename) or do {
        print "\nNo MP3 info for $filename (the file is likely to be unplayable).";
        return;
      };
      my $mp3tag = get_mp3tag($filename) or do {
        print "\nNo ID3 tag info for $filename";
        return;
      };
      $tartist = $mp3tag->{'ARTIST'};
      $talbum  = $mp3tag->{'ALBUM'};
      $ttitle  = $mp3tag->{'TITLE'};
      $tyear   = $mp3tag->{'YEAR'};
      $tnum    = $mp3tag->{'TRACKNUM'};
      $tlength  = sprintf( "%02dm:%02d.%03ds", $mp3info->{'MM'}, $mp3info->{'SS'}, $mp3info->{'MS'} );
      $tquality = sprintf( "MP3: %s kbps, %s kHz, %s", $mp3info->{'BITRATE'}, $mp3info->{'FREQUENCY'}, ($mp3info->{'STEREO'} ? "stereo" : "mono") );

    }
    else {
      # Get the info out of the (presumed) .ogg file
      my $ogghdr = Ogg::Vorbis::Header->load($filename) or do {
        print "\nNo Ogg Vorbis info for $filename";
        return;
      };
      $tlength  = sprintf( "%.1lf", $ogghdr->info('length'));
      my $average_bitrate = ($fstats[7] / 125 ) / $tlength ;
      $tquality = sprintf( "OGG: %0.0lf kbps, %0.1lf kHz, %s", $average_bitrate, $ogghdr->info('rate')/1000, ($ogghdr->info('channels') > 1 ? "stereo" : "mono"));
      $tlength  = sprintf( "%.1lf", $ogghdr->info('length'));

      foreach my $key ($ogghdr->comment_tags) {
        foreach my $value ( $ogghdr->comment($key) ) {
          printf( "[%s] = [%s]\n", $key, $value ) if ( $opts{'debug'} );
          if    ( $key =~ /^artist$/i   ) { $tartist = $value; }
          elsif ( $key =~ /^album$/i    ) { $talbum  = $value; }
          elsif ( $key =~ /^title$/i    ) { $ttitle  = $value; }
          elsif ( $key =~ /^tracknum(ber)?$/i ) { $tnum    = $value; }
          elsif ( $key =~ /^date$/i     ) { $tyear   = $value; }
        }
      }
    }

    # Now adjust some of those values a little further...
    $tnum =~ s/\/.*$//;  # in case it is 7/12 format (i.e. track 7 of 12).
    $tnum =~ s/[^0-9]//g ;
    $tnum = 0 if ( $tnum eq "" );
    $tnum = int($tnum);
    $setpart = int($setpart);

    my @query_args = ( $tartist, $talbum, $tnum, $ttitle, $tyear );
    printf( "[%s]\n", join( '], [', @query_args )) if ( $opts{'debug'} );

    $insert_track->execute( $hashkey, $safe_path, $ttitle, $tartist, $talbum, $tnum, $setpart, $tlength, $tquality ) or
        die $dbh->errstr;

  }
}


#################################################################
# Recurse through a single directory
#################################################################
sub one_directory {
  my ( $dirname ) = @_;
  my @files;

  return if ( $dirname =~ /^abcde\./ );  # Skip random stuff abcde leaves around.

  print "\nLooking in: $dirname" unless( $opts{'quiet'} );
  if ( $opts{'clean'} ) {
    $clean_tracks_by_path->execute( $dirname.'%' ) or die $dbh->errstr;
  }

  opendir( DIR, $dirname );
  @files = grep { ! /^\./ } readdir(DIR);
  closedir DIR;

  foreach ( sort @files ) {
    my $full_name = "$dirname/$_";
    next if ( -l $full_name );     # Skip symbolic links
    if ( -f $full_name ) {
      one_file( $full_name );
    }
    elsif ( -d $full_name ) {
      one_directory( $full_name );
    }
  }
}



#################################################################
# Main code...
#
#################################################################
if ( $#file_list < 0 ) {
  push @file_list, cwd();
}

foreach ( @file_list ) {
  s/\/$// ;

  my $abs_path = abs_path($_);

  if ( -f $abs_path ) {
    one_file( $abs_path );
  }
  elsif( -d $abs_path ) {
    one_directory( $abs_path );
  }
}


print "\nDealt to $processed_count files.\n";
