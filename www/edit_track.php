<?php
  $refresh = false;
  $page_title = "Edit Track Information";
  include("always.php");

  if ( isset( $submit ) && $submit == "update" ) {
    // update the track
    // $actions = "<p>Updating tracks";
    for ( $i=0 ; isset( $hash[$i] ); $i++ ) {
      $q = "SELECT * FROM tracks WHERE hash_key='$hash[$i]';";
      $res = awm_pgexec( $dbconn, $q );
      // $actions .= "<p>Query: $q";
      if ( $res && pg_NumRows( $res) == 1 ) {
        $track = pg_Fetch_Object( $res, 0 );
        // $actions .= "<p>Editing: $track->title";
        if ( ! isset( $artist[$i] ) ) $artist[$i] = $artist[0];
        if ( ! isset( $album[$i] ) ) $album[$i] = $album[0];
        if ( preg_match( '{\.ogg$}i', $track->path_name ) ) {
          // Update the details on an ogg file
          $command = "/usr/bin/vorbiscomment -w " . escapeshellarg($track->path_name);
          $command .= " -t " . escapeshellarg( "ARTIST=$artist[$i]" );
          $command .= " -t " . escapeshellarg( "ALBUM=$album[$i]" );
          $command .= " -t " . escapeshellarg( "TITLE=$title[$i]" );
          $command .= " -t " . escapeshellarg( "TRACKNUMBER=$tracknum[$i]" );
        }
        else {
          // Update the details on an mp3 file
          $command = "/usr/bin/id3v2 -a " . escapeshellarg($artist[$i]) . " -A " . escapeshellarg($album[$i]);
          $command .= " -t " . escapeshellarg($title[$i]) . " -T " . escapeshellarg($tracknum[$i]) . " ". escapeshellarg($track->path_name);
        }
        // $actions .= "<p>Command: $command";
        $result = `$command`;
        if ( preg_match( '{error.*opening}i', $result ) ) {
          $actions .= "<p class=error>Error opening file - are your files / directories mode 666 / 777?";
        }
        $command = "/usr/bin/md5sum " . escapeshellarg($track->path_name);
        $md5 = `$command`;
        // $actions .= "<p>Command: $command";
        list( $newhashkey, $track) = explode( " ", trim($md5), 2);
        $q = "UPDATE played SET hash_key = '$newhashkey' WHERE hash_key = '" . pg_escape_string($hash[$i]) . "'; ";
        $q .= "UPDATE tracks SET hash_key = '$newhashkey', ";
        $q .= "artist = '" . pg_escape_string($artist[$i]) . "', ";
        $q .= "album = '" . pg_escape_string($album[$i]) . "', ";
        $q .= "title = '" . pg_escape_string($title[$i]) . "', ";
        $q .= "tracknum = '" . pg_escape_string($tracknum[$i]) . "' ";
        $q .= "WHERE hash_key = '" . pg_escape_string($hash[$i]) . "'; ";
        $res = awm_pgexec( $dbconn, $q );
        // $actions .= "<p>Query: $q";
        $actions .= "<p>Updated $title[$i]";
      }
      else if ( $res ) {
        $actions .= "<p class=error>Looks like the hash $hash[$i] is insufficiently unique!";
      }
      else {
        $actions .= "<p class=error>No tracks found for hash $hash[$i] !";
      }
    }
  }

  include("header.php");

  echo "$actions";

  $letter_get = ( "$altr" == "" ? "" : "&altr=$altr" ) . ( "$lltr" == "" ? "" : "&lltr=$lltr" );

function show_edit( $i, $trk ) {
  echo "<input type=hidden name=hash[$i] value=$trk->hash_key>\n";
  if ( $i == 0 ) {
    echo "<tr><td colspan=\"4\"><hr style=\"height: 5px;\"></td></tr>\n";
    echo "<tr><th align=right>Artist:</th>";
    echo "<td colspan=\"3\"><input type=text size=60 name=artist[$i] value=\"" . htmlentities($trk->artist) . "\"></td></tr>\n";
    echo "<tr><th align=right>Album:</th>";
    echo "<td colspan=\"3\"><input type=text size=60 name=album[$i] value=\"" . htmlentities($trk->album) . "\"></td></tr>\n";
    echo "<tr><td colspan=\"4\"><hr style=\"height: 5px;\"></td></tr>\n";
  }
  $class = sprintf( 'class="row%d"', $i%2 );
  $prompt_fmt = sprintf( '<th %s align="right">%%s:</th><td %s><input type="text" size="%%d" name="%%s" value="%%s">%%s</td>', $class, $class );
  $row = sprintf( "<tr %s>%s%s</tr>", $class, $prompt_fmt, $prompt_fmt );
  printf( $row, "Number", 3, "tracknum[$i]", $trk->tracknum, "",
                "Title", 40, "title[$i]", htmlentities($trk->title), " ".$trk->path_name );
  echo "<tr><td colspan=\"4\"><hr></td></tr>\n";
}

  if ( isset($l) && isset($a) ) {
    $q = "SELECT * FROM tracks WHERE album = '" . addslashes($l) . "' AND artist = '" . addslashes($a) . "' ";
    if ( isset($t) ) {
      $q .= "AND title='" . addslashes($t) . "' ";
    }
    $q .= "ORDER BY tracknum; ";
    $res = awm_pgexec( $dbconn, $q, "db");
    if ( $res && pg_NumRows($res) > 0 ) {
      echo "<form method=post>\n";
      echo "<table width=100%>\n";
      for ( $i=0; $i < pg_NumRows($res); $i++ ) {
        $track = pg_Fetch_Object( $res, $i );
        show_edit( $i, $track );
      }
      echo "<tr><td>&nbsp;</td><td align=left><input type=submit name=submit value=update></td></tr>\n";
      echo "</table>\n";
      echo "</form>\n";
    }
  }
  else {
    echo "<h1>Error: No Album Selected</h1>
    <p>You will need to select an album or a track for editing";
  }

  show_queue();
?>

</body>
</html>
