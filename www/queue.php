<?php
  include('always.php');

header( 'Content-type: text/plain');
header( 'Expires: 10s');

function track_link( $trk, $link_title, $row_class = "" ) {
  $track_link_url = sprintf( "?l=%s&a=%s&t=%s", urlencode($trk->album),
                                     urlencode($trk->artist), urlencode($trk->title) );
  $safe_title = htmlspecialchars($link_title);
  $duration = preg_replace( "/^[0:]{1,4}/", "", $trk->duration );
  $link = <<<EOHTML
<tr class="track$row_class">
  <td class="track$row_class">
    <a class="track$row_class" href="artist.php$track_link_url" title="$trk->path_name">$safe_title</a>
  </td>
  <td class="track$row_class">$duration</td>
  <th class="track$row_class">
    <a class="alphabetica" href="edit_track.php$track_link_url" title="Edit Track Info">E</a>
  </th>
</tr>

EOHTML;

  return $link;
}


$album_artist_fmt = <<<EOHTML

<tr class="q_headline">
  <td class="q_cell" colspan="4">
    %s
  </td>
</tr>
EOHTML;

$artist_link_fmt = '<a href="/?type=artist&a=%s"><span class="q_artist">%s</span></a>';
$album_link_fmt  = '<a href="/?type=album&l=%s"><span class="q_album">%s</span></a>';

$track_fmt = <<<EOHTML

<tr class="q_row r%d">
  <td class="q_cell" title="Position in queue">%d</td>
  <td class="q_title" title="Track number and name">%d - %s</td>
  <td class="q_cell" title="When this track will start playing">%s</td>
  <td class="q_cell"><a href="/?i=%d&d=up"><img alt="up" src="/img/up.png"></a> <a href="/?i=%d&d=dn"><img alt="down" src="/img/down.png"></a></td>
</tr>
EOHTML;

$current_track = current_track();
$tracks = current_queue();

// print_r( $tracks );
// print_r($current_track);

$last_artist = "";
$last_album = "";
$when = $current_track->finishing;
foreach( $tracks AS $k => $v ) {
  if ( $k == 0 ) $when -= $v->dur_secs;
  $escape_artist = urlencode($v->artist);
  $escape_album  = urlencode($v->album);


  if ( $escape_artist != $last_artist || $escape_album != $last_album ) {
    if ( $escape_artist != $last_artist && $escape_album != $last_album ) {
      $links = sprintf( $album_link_fmt, $escape_album, $v->album);
     $links .= "/" . sprintf( $artist_link_fmt, $escape_artist, $v->artist );
    }
    else if ( $escape_artist != $last_artist ) {
     $links = sprintf( $artist_link_fmt, $escape_artist, $v->artist );
    }
    else if ( $escape_album != $last_album ) {
      $links = sprintf( $album_link_fmt, $escape_album, $v->album);
    }
    printf( $album_artist_fmt, $links );
    $last_artist = $escape_artist;
    $last_album = $escape_album;
  }
  printf( $track_fmt, ($k %2), $k, $v->tracknum,
                         $v->title, date('H:i:s', $when ), $k, $k );
  $when += $v->dur_secs;

}
