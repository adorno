<?php
  unset($altr);  if ( isset($_GET['altr']) ) $altr = $_GET['altr'];
  unset($lltr);  if ( isset($_GET['lltr']) ) $lltr = $_GET['lltr'];
  unset($a);  if ( isset($_GET['a']) ) $a = $_GET['a'];
  unset($l);  if ( isset($_GET['l']) ) $l = $_GET['l'];

  require_once("always.php");
  require_once("Session.php");
  require_once("PlayTracks.php");

  $title = $system_name;
  require_once("header.php");


  if ( "$error_loc$error_msg" == "" ) {

function track_link( $trk, $link_title, $row_class = "" ) {
  global $letter_get, $search;

  $url_artist = urlencode($trk->artist);
  $track_link_url = sprintf( "?l=%s&a=%s&t=%s", urlencode($trk->album), $url_artist, urlencode($trk->title) );

  $link_title = ($trk->tracknum > 0 ? "$trk->tracknum: " : "") . $trk->title;
  $safe_title = htmlspecialchars($link_title);
  $safe_artist = htmlspecialchars($trk->artist);

  if ( isset($search) ) {
    $delimiter_list = '/#~`!@$%^&*_-=|';
    for( $i=0; strpos($search,substr($delimiter_list,$i,1)) !== false; $i++ );
    $delimiter = substr($delimiter_list,$i,1);
    if ( preg_match($delimiter.$search.$delimiter.'i', $link_title) || preg_match($delimiter.$search.$delimiter.'i', $trk->artist) ) {
      $row_class = "found";
    }
  }

  $duration = preg_replace( "/^[0:]{1,4}/", "", $trk->duration );
  $link = <<<EOHTML
<tr class="track$row_class">
  <td class="track$row_class">
    <a class="track$row_class" href="artist.php$track_link_url$letter_get" title="$trk->path_name">$safe_title</a>
    /
    <a class="track$row_class" href="artist.php?a=$url_artist$letter_get" title="Show tracks by $trk->artist">$safe_artist</a>
  </td>
  <td class="track$row_class">$duration</td>
  <th class="track$row_class">
    <a class="alphabetica" href="edit_track.php$track_link_url" title="Edit Track Info">E</a>
  </th>
</tr>

EOHTML;

  return $link;
}


    echo "<table width=\"100%\"><tr valign=\"top\">\n";

    echo "<td width=\"40%\">\n";
    if ( isset($a) ) {
      $qa = "SELECT artist, album, title, tracknum, path_name, duration::interval(0), ";
      $qa .= "extract( EPOCH FROM duration)::int AS secs, quality ";
      $qa .= "FROM tracks WHERE lower(artist) = lower('" . addslashes($a) . "') ";
      $qa .= "ORDER BY lower(album), setpart, tracknum; ";
      $qry = new AwlQuery($qa);
      if ( $qry->Exec() && $qry->rows() > 0 ) {
        echo "<h3>".htmlspecialchars($a)."</h3>\n";
        $last_album = null;
        $rownum = 0;
        while ( $track = $qry->Fetch() ) {
          if ( $track->album != $last_album ) {
            if ( $rownum > 1 ) echo "</table>";
            echo "<br><table width=100% cellspacing=0 cellpadding=0><tr class=th4>";
            echo "<a class=th4 href=\"album.php?a=" . urlencode($track->artist) . "&l=" . urlencode($track->album) . "$letter_get\">";
	    echo "<td width=86% class=th4>";
	    echo htmlspecialchars($track->album);
	    echo "</td>\n";
	    echo "</a>";
            echo "<td width=10% class=th4>&nbsp;</td>";
            echo "<td width=4% class=th4><a class=alphabetica  class=track href=\"edit_track.php?l=" . urlencode($track->album) . "&a=" . urlencode($track->artist) . "\" title=\"Edit Album Info\">E</a></td></tr>";
            $last_album = $track->album;
          }
          else if ( $rownum == 0 ) {
            echo "<br><table width=100%><tr><td width=96% class=h4>";
            echo "<h4>Unkown Album</h4></td><td width=4%>&nbsp;</td></tr>\n";
          }
          echo track_link($track, ($track->tracknum > 0 ? "$track->tracknum: " : "") . "$track->title",  $rownum % 2 );
          $rownum++;
        }
        echo "</table>";
      }
    }
    else if ( isset( $l ) ) {
      $qa = "SELECT artist, album, title, tracknum, path_name, duration::interval(0), ";
      $qa .= "extract( EPOCH FROM duration)::int AS secs, quality ";
      $qa .= "FROM tracks WHERE album = '" . addslashes($l) . "' ";
      $qa .= "ORDER BY album, setpart, tracknum; ";
      $qry = new AwlQuery($qa);
      if ( $qry->Exec() && $qry->rows() > 0 ) {
        echo "<h3><a class='th4' title='Add this whole album to the playlist.' href=\"album.php?play=1&l=" . urlencode($l) . "$letter_get\">$l</a></h3>\n";
        $last_album = "";
        $rownum = 0;
        while ( $track = $qry->Fetch() ) {
          if ( $rownum == 0 ) {
            echo "<br /><table width=100%>";
          }
          echo track_link($track, ($track->tracknum > 0 ? "$track->tracknum: " : "") . "$track->title / $track->artist",  $rownum % 2 );
          $rownum++;
        }
        echo "</table>";
      }
    }
    else {
      echo "&nbsp;";
    }
    echo "</td>";

    echo "<td width=\"60%\">\n";
    echo "<h3>Albums</h3>\n<p>";
    if ( isset($altr) || isset($search) ) {
      $altr = trim("$altr");
      if ( isset($search) ) {
        $sql = "SELECT distinct ON ( lower(album)) album FROM tracks ";
        $sql .= "WHERE artist ~* ? OR album ~* ? OR title ~* ? OR path_name ~* ? ";
        $sql .= "ORDER BY lower(album);";
        $qry = new AwlQuery( $sql, $search, $search, $search, $search );
      }
      else {
        $qry = new AwlQuery( "SELECT distinct ON ( lower(album)) album FROM tracks WHERE album ~* ? ORDER BY lower(album);", "^$altr");
      }
      if ( $qry->Exec('album') && $qry->rows() > 0 ) {
        while ( $album = $qry->Fetch() ) {
          $display = htmlspecialchars($album->album);
          if ( trim($display) == "" ) $display = "&laquo;unknown&raquo;";
          echo " <a href=\"album.php?l=" . urlencode($album->album) . "$letter_get\" class=\"artist\">$display</a>\n";
        }
      }
    }
    echo "</td>\n";

    echo "</tr></table>\n";
  }

  include("footers.php");
?>
