<?php
/**
* @package adorno
* @author Andrew McMillan <andrew@mcmillan.net.nz>
* @copyright Morphoss Ltd <http://www.morphoss.com/>
* @license   http://gnu.org/copyleft/gpl.html GNU GPL v2 or later
*/

if ( preg_match('{/always.php$}', $_SERVER['SCRIPT_NAME'] ) ) header('Location: index.php');

// Ensure the configuration starts out as an empty object.
$c = (object) array();
$c->script_start_time = microtime(true);

// Ditto for a few other global things
unset($session); unset($request); unset($dbconn); unset($_awl_dbconn); unset($include_properties);

// An ultra-simple exception handler to catch errors that occur
// before we get a more functional exception handler in place...
function early_exception_handler($e) {
  echo "Uncaught early exception: ", $e->getMessage(), "\nAt line ", $e->getLine(), " of ", $e->getFile(), "\n";

  $trace = array_reverse($e->getTrace());
  foreach( $trace AS $k => $v ) {
    printf( "=====================================================\n%s[%d] %s%s%s()\n", $v['file'], $v['line'], (isset($v['class'])?$v['class']:''), (isset($v['type'])?$v['type']:''), (isset($v['function'])?$v['function']:'') );
  }
}
set_exception_handler('early_exception_handler');


$c->system_name = 'Adorno Headless Music Server';
$c->daemon_type = 'adorno';

error_log( "=============================================== $PHP_SELF" );

// Utilities
if ( ! @include_once('AWLUtilities.php') ) {
  $try_paths = array(
        '../../awl/inc'
      , '/usr/share/awl/inc'
      , '/usr/local/share/awl/inc'
  );
  foreach( $try_paths AS $awl_include_path ) {
    if ( @file_exists($awl_include_path.'/AWLUtilities.php') ) {
      set_include_path( $awl_include_path. PATH_SEPARATOR. get_include_path());
      break;
    }
  }
  if ( ! @include_once('AWLUtilities.php') ) {
    echo "Could not find the AWL libraries. Are they installed? Check your include_path in php.ini!\n";
    exit;
  }
}

// Ensure that ../inc is in our included paths as early as possible
set_include_path( '../inc'. PATH_SEPARATOR. get_include_path());


/**
* We use @file_exists because things like open_basedir might noisily deny
* access which could break DAViCal completely by causing output to start
* too early.
*/
if ( @file_exists('/etc/adorno/'.$_SERVER['SERVER_NAME'].'-conf.php') ) {
  include('/etc/adorno/'.$_SERVER['SERVER_NAME'].'-conf.php');
}
else if ( @file_exists('/etc/adorno/config.php') ) {
  include('/etc/adorno/config.php');
}
else if ( @file_exists('/usr/local/etc/adorno/'.$_SERVER['SERVER_NAME'].'-conf.php') ) {
  include('/usr/local/etc/adorno/'.$_SERVER['SERVER_NAME'].'-conf.php');
}
else if ( @file_exists('/usr/local/etc/adorno/config.php') ) {
  include('/usr/local/etc/adorno/config.php');
}
else if ( @file_exists('../config/config.php') ) {
  include('../config/config.php');
}
else if ( @file_exists('config/config.php') ) {
  include('config/config.php');
}
else {
  include('adorno_configuration_missing.php');
  exit;
}

if ( !isset($c->page_title) ) $c->page_title = $c->system_name;


param_to_global('altr','{^[A-Z#]$}');
param_to_global('lltr','{^[A-Z#]$}');
param_to_global('search','{^.*$}');
param_to_global('action','{^(pause|enqueue|remove|resume|next|clear|off|stop)$}');
param_to_global('submit','{^.*$}');
param_to_global('a','{^.*$}');
param_to_global('l','{^.*$}');
param_to_global('t','{^.*$}');
param_to_global( 'ltrtype', '{(artist|album|genre|stream)}', 'type' );


function build_url_params( $url_params = array() ) {
  $std_params = array( 'ltrtype', 'altr', 'lltr', 'a', 'search' );
  foreach( $std_params AS $p ) {
    if ( !isset($url_params[$p]) && isset($GLOBALS[$p])
          && ! ($p == 'ltrtype' && isset($url_params['type'])) ) {
      $url_params[($p == 'ltrtype'?'type':$p)] = $GLOBALS[$p];
    }
  }
  $params = array();
  foreach( $url_params AS $k => $v ) {
    $params[] = $k .'=' . rawurlencode($v);
  }
  $params = implode( $params, '&' );
  if ( $params != '' ) $params = '?' . $params;
  return $params;
}


$letter_get = ( "$altr" == "" ? "" : "&altr=$altr" ) . ( "$lltr" == "" ? "" : "&lltr=$lltr" );
if ( isset($search) && $search == '' ) $search = null;
if ( isset($search) ) $letter_get .= "&search=".urlencode($search);


function nice_track_name( $track ) {
  $track = preg_replace( "#^/#", "", $track );
  $track = preg_replace( "#^music/#", "", $track );
  $track = preg_replace( "#^extra/#", "", $track );
  $track = preg_replace( "#^Classified/#", "", $track );
  $track = str_replace( "http://", "Stream/ ", $track );
  $track = preg_replace( "#[.](mp3|ogg)$#i", "", $track );
  $track = preg_replace( "#^/+#", "", $track );
  $track = preg_replace( "#/+$#", "", $track );
  $track = str_replace( "_", " ", $track );
  $track = str_replace( "/", " &raquo; ", $track );
  return $track;
}

include("daemonInterface.php");
