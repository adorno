<?php
include_once("always.php");
  $current_track = current_track();
  if ( is_object($current_track) ) {
    $track_information = "$current_track->artist : $current_track->album : $current_track->title ";
    $track_information .= sprintf( ": %s : %s", date( "H:i:s" ), date( "H:i:s", $current_track->finishing ) );
    error_log( "$sysabbr: DBG: Title >>$page_title<<   Finish >>$current_track->finishing >>".time()." >>$refresh_time");
  }
  echo $track_information;
?>