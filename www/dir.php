<?php
  include("always.php");
  $dd = urlencode("$d");
?>
<html>
<head>
<title>Adorno - Directory of <?php echo str_replace("&nbsp;","",nice_track_name("$d")); ?></title>
</head>
<body bgcolor=white>
<table width=100%><tr>
<td><h2>Directory of <?php echo nice_track_name("$d"); ?></h2></td>
<td align=right><a href="/?submit=a&action=next">NEXT</a> &nbsp; | &nbsp;
<a href="/dir.php?d=$dd&submit=a&action=clear">CLEAR</a> &nbsp; | &nbsp;
<a href="/dir.php?d=$dd&submit=a&action=quit">QUIT</a> &nbsp; | &nbsp;
<a href="/dir.php?d=$dd&submit=a&action=off">OFF</a> &nbsp; | &nbsp;
<a href="/">TOP</a>
</td></tr></table>
<hr>

<?php


  $dirname = preg_replace( '{\/*$}i', "", "/music" . "$d" );
  $dirname = "/music" . "$d";
  $tracklist = array();
  $dirlist = array();
  $md5list = array();
  $in_database = false;
  if ( $dir = opendir( "$dirname" ) ) {
    while (($file = readdir($dir)) !== false) {
      if ( is_dir( "$dirname/$file" ) ) {
        if ( preg_match( '{^\.}', $file ) ) continue;
        array_push( $dirlist, $file );
      }
      else {
        if ( !preg_match( '{\.(mp3)|(ogg)$}', $file ) ) continue;
        array_push( $tracklist, $file );
        if ( ! $in_database ) {
          $md5 = `/usr/bin/md5sum "$dirname$file"`;
          list( $hashkey, $track) = explode( " ", trim($md5), 2);
          $query =  "SELECT * FROM tracks WHERE hash_key = '$hashkey';";
          $result = pg_Exec( $dbconn, $query);
          if ( $result && pg_NumRows($result) > 0 ) {
            echo "$track was found in database<br>\n";
            $in_database = true;
          }
          else {
            $md5 = `/usr/bin/md5sum "$dirname$file"`;
            array_push( $md5list, $md5 );
          }
        }
      }
    }
    closedir($dir);
  }

  $file = preg_replace( '{/[^/]+/$}', "/", $d);
  echo "<p><a href=\"/dir.php?d=$file\">Up one level</a><br>\n";

  sort ($dirlist);
  reset ($dirlist);
  while (list ($key, $val) = each ($dirlist)) {
    // echo " &nbsp; <a href=\"/dir.php?d=$d&submit=a&action=queue&track=$d$val\">$val</a><br>\n";
    echo "<a href=\"/dir.php?d=$d" . urlencode($val) . "/\">" . str_replace(" ", "&nbsp;", $val) . "</a> &nbsp; \n";
  }
  echo "</p>\n";

  echo "<hr>\n";

  if ( count($tracklist) > 0 ) {
    echo "<p><b>Select tracks to Play</b><br>\n";
    sort ($tracklist);
    reset ($tracklist);
    while (list ($key, $val) = each ($tracklist)) {
      echo " &nbsp; <a href=\"/dir.php?d=$dd&submit=a&action=queue&track=$dd" . urlencode($val) . "\">$val</a><br>\n";
    }
    echo "</p>\n";
  }

  if ( count($md5list) > 0 ) {
    echo "<p><b>Database List</b><br>\n";
    reset ($md5list);
    while (list ($key, $val) = each ($md5list)) {
      list( $hashkey, $track) = explode( " ", trim($val), 2);
      $query =  "SELECT * FROM tracks WHERE hash_key = '$hashkey';";
      $result = pg_Exec( $dbconn, $query);
      if ( $result && pg_NumRows($result) > 0 ) {
        echo "$track was found in database<br>\n";
      }
      else {
        $track = trim($track);
        echo "$track added into database<br>\n";
        $safe_track = preg_replace( '{^.*/([^/]+)$}', "\\1", str_replace( "'", "''", $track ));
        $safe_path = str_replace( "'", "''", $track );
        if ( preg_match( '{\.mp3$}i', $track ) ) {
          $command = "mp3info -p '%n\t%t\t%l\t%a' '$track'";  // track number >> track title >> album name >> artist name
//          error_log( "mdaemon: $command", 0);
          $mp3info = `$command`;  // track number >> track title >> album name >> artist name
//          error_log( "mdaemon: $mp3info", 0);
          list( $tnum, $ttitle, $talbum, $tartist ) = split( "\t", $mp3info, 4);
          $tnum = intval($tnum);
          $ttitle = str_replace( "'", "''", $ttitle );
          $talbum = str_replace( "'", "''", $talbum );
          $tartist = str_replace( "'", "''", $tartist );
          $query =  "INSERT INTO tracks ( hash_key, path_name, title, artist, album, tracknum ) ";
          $query .= "VALUES( '$hashkey', '$safe_path', '$ttitle', '$tartist', '$talbum', '$tnum') ;";
        }
        else if ( preg_match( '{\.ogg$}i', $track ) ) {
          $ogginfo = `ogginfo $track`;

// serial=1994287629
// header_integrity=pass
// ARTIST=Cranberries
// ALBUM=Bury the Hatchett
// TITLE=Fee Fi Fo
// TRACKNUMBER=12
// vendor=Xiphophorus libVorbis I 20011231
// version=0
// channels=2
// rate=44100
// bitrate_upper=none
// bitrate_nominal=112015
// bitrate_lower=none
// stream_integrity=pass
// bitrate_average=114037
// length=287.133333
// playtime=4:47
// stream_truncated=false

          $trackinfo = split( "\n", $ogginfo);
	  while( list( $k , $v ) = each( $trackinfo ) ) {
	    list( $type, $value ) = split( "=", $v, 2);
	    switch ( strtoupper($type) ) {
	      case 'ARTIST':      $tartist = $value; break;
	      case 'ALBUM':       $talbum  = $value; break;
	      case 'TITLE':       $ttitle  = $value; break;
	      case 'TRACKNUMBER': $tnum    = $value; break;
	    }
	  }
          $tnum = intval($tnum);
          $ttitle = str_replace( "'", "''", $ttitle );
          $talbum = str_replace( "'", "''", $talbum );
          $tartist = str_replace( "'", "''", $tartist );

          $query =  "INSERT INTO tracks ( hash_key, path_name, title, artist, album, tracknum ) ";
          $query .= "VALUES( '$hashkey', '$safe_path', '$ttitle', '$tartist', '$talbum', '$tnum') ;";
//          $query =  "INSERT INTO tracks ( hash_key, path_name, title ) VALUES( '$hashkey', '$safe_path', '$safe_track') ;";
        }
        else {
          $query =  "INSERT INTO tracks ( hash_key, path_name, title ) VALUES( '$hashkey', '$safe_path', '$safe_track') ;";
        }
        $result = pg_Exec( $dbconn, $query);
      }
//      echo " &nbsp; <a href=\"/dir.php?d=$d&submit=a&action=queue&track=$d$val\">$val</a><br>\n";
    }
  }

  show_queue();

?>

</body>
</html>
