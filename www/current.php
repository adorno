<?php
  include('always.php');
  $current_track = current_track();

  if ( is_object($current_track) ) {
    if ( $current_track->state == 'Stopped' ) {
      echo 'Nothing is currently playing';
    }
    else {
      if ( $current_track->state == 'Paused' ) echo '<span class="paused">';
      printf( '<span class="track_title">%s</span>
<span class="album_title">%s</span>
<span class="artist_name">%s</span>
<span class="finish_time">%s</span>
', $current_track->title, $current_track->album,
        $current_track->artist,
        date("H:i:s",$current_track->finishing));
      if ( $current_track->state == 'Paused' ) echo ' (paused)</span>';
    }
  }
