<?php
  include("always.php");
  include("header.php");

  $letter_get = ( "$altr" == "" ? "" : "&altr=$altr" ) . ( "$lltr" == "" ? "" : "&lltr=$lltr" );

function track_link( $trk, $link_title, $row_class = "" ) {
  global $letter_get;
  $link = "<tr class=track$row_class><td class=track$row_class><a class=track$row_class href=\"db.php?l=" . urlencode($trk->album) . "&a=" . urlencode($trk->artist) . "&t=" . urlencode($trk->title) . "$letter_get\" title=\"$trk->path_name\">";
//  $link .= str_replace( " ", "&nbsp;", $link_title);
  $link .= $link_title;
  $link .= "</a></td>";
  $link .= "<th class=track><a class=alphabetica  class=track$row_class href=\"edit_track.php?l=" . urlencode($trk->album) . "&a=" . urlencode($trk->artist) . "&t=" . urlencode($trk->title) . "\" title=\"Edit Track Info\">E</a></th>";
  $link .= "</tr>\n";

  return $link;
}

  if ( isset($l) && isset($a) && isset($t) ) {
    $q = "SELECT * FROM tracks WHERE album = '" . addslashes($l) . "' AND artist = '" . addslashes($a) . "' AND title='" . addslashes($t) . "' ";
    $res = awm_pgexec( $dbconn, $q, "db");
    if ( $res && pg_NumRows($res) > 0 ) {
      $track = pg_Fetch_Object( $res, 0 );
      $fifo = fopen( "/var/run/adorno/fifo", "w" );
      fputs( $fifo, "queue $track->path_name\n" );
      fclose($fifo);
      $q = "SELECT track_played( '" . addslashes($track->hash_key) . "'); ";
      $res = awm_pgexec( $dbconn, $q, "db");
    }
  }

  echo "<table><tr valign=top>\n";
  $widthused=0;

  if ( isset($l) || isset($a) || isset($t) ) {
    echo "<td width=40%>\n";
    $widthused += 40;
    if ( (isset($a) || isset($play)) && isset($l) && ! isset($t) ) {
      // Play all of the tracks for this alum
      $q = "SELECT * FROM tracks WHERE album = '" . addslashes($l) . "' ";
      if ( isset($a) ) $q .= "AND artist = '" . addslashes($a) . "'";
      $q .= "ORDER BY setpart, tracknum; ";
      $res = awm_pgexec( $dbconn, $q, "db");
      if ( $res && pg_NumRows($res) > 0 ) {
        $fifo = fopen( "/var/run/adorno/fifo", "w" );
        $q = "";
        for ( $i=0; $i < pg_NumRows($res); $i ++ ) {
          $track = pg_Fetch_Object( $res, $i );
          fputs( $fifo, "queue $track->path_name\n" );
          $q .= "SELECT track_played( '" . addslashes($track->hash_key) . "'); ";
        }
        fclose($fifo);
        $res = awm_pgexec( $dbconn, $q, "db");
      }
    }

    if ( isset($a) ) {
      $qa = "SELECT * FROM tracks WHERE artist = '" . addslashes($a) . "' ";
      $qa .= "ORDER BY album, setpart, tracknum; ";
      $res = awm_pgexec( $dbconn, $qa, "db");
      if ( $res && pg_NumRows($res) > 0 ) {
        echo "<h3>$a</h3>\n";
        $last_album = "";
        for ( $i = 0; $i < pg_NumRows($res); $i++ ) {
          $track = pg_Fetch_Object( $res, $i );
          if ( $track->album != "" && $track->album != $last_album ) {
            if ( $i > 1 ) echo "</table>";
            echo "<br><table width=100% cellspacing=0 cellpadding=0><tr class=th4><td width=96% class=th4>";
            echo "<a class=th4 href=\"db.php?a=" . urlencode($track->artist) . "&l=" . urlencode($track->album) . "$letter_get\">$track->album</a></td>\n";
            echo "<td width=4% class=th4><a class=alphabetica  class=track href=\"edit_track.php?l=" . urlencode($track->album) . "&a=" . urlencode($track->artist) . "\" title=\"Edit Album Info\">E</a></td></tr>";
            $last_album = $track->album;
          }
          else if ( $i == 0 ) {
            echo "<br><table width=100%><tr><td width=96% class=h4>";
            echo "<h4>Unkown Album</h4></td><td width=4%>&nbsp;</td></tr>\n";
          }
          // echo track_link($track, ($track->tracknum > 0 ? "$track->tracknum: " : "") . "$track->title",  " bgcolor=" . $colors["bg".($i%2)] );
          echo track_link($track, ($track->tracknum > 0 ? "$track->tracknum: " : "") . "$track->title",  $i % 2 );
        }
        echo "</table>";
      }
    }
    else if ( isset( $l ) ) {
      $qa = "SELECT * FROM tracks WHERE album = '" . addslashes($l) . "' ";
      $qa .= "ORDER BY album, setpart, tracknum; ";
      $res = awm_pgexec( $dbconn, $qa, "db");
      if ( $res && pg_NumRows($res) > 0 ) {
        echo "<h3><a class=th4 href=\"db.php?play=1&l=" . urlencode($l) . "$letter_get\">$l</a></h3>\n";
        $last_artist = "";
        for ( $i = 0; $i < pg_NumRows($res); $i++ ) {
          $track = pg_Fetch_Object( $res, $i );
          if ( $track->artist != "" && $track->artist != $last_artist ) {
            echo "<br><table width=100% cellspacing=0 cellpadding=0><tr class=th4><td width=96% class=th4>";
            echo "<a class=h4 href=\"db.php?a=" . urlencode($track->artist) . "&l=" . urlencode($track->album) . "$letter_get\">$track->artist</a></td>\n";
            echo "<td width=4% class=th4><a class=alphabetica  class=track href=\"edit_track.php?l=" . urlencode($track->album) . "&a=" . urlencode($track->artist) . "\" title=\"Edit Album Info\">E</a></td></tr>";
            $last_artist = $track->artist;
          }
          else if ( $i == 0 ) {
            echo "<br><table width=100%><tr><td width=96% class=h4>";
            echo "<h4>Unkown Album</h4></td><td width=4%>&nbsp;</td></tr>\n";
          }
          echo track_link($track, ($track->tracknum > 0 ? "$track->tracknum: " : "") . "$track->title",  $i % 2 );
        }
        echo "</table>";
      }
    }
  }

    echo "<td width=" . ((100 - $widthused) / 2) . "%>\n";
    echo "<h3>Artists</h3>\n<p>";
    $qa = "SELECT distinct upper(substring(artist from 1 for 1)) AS altr FROM tracks ORDER BY 1;";
    $res = awm_pgexec( $dbconn, $qa, "db");
    if ( $res && pg_NumRows($res) > 0 ) {
      for ( $i = 0; $i < pg_NumRows($res); $i++ ) {
        $artist = pg_Fetch_Object( $res, $i );
        if ( "$artist->altr" == "" ) continue;
        echo "<a class=alphabetica href=\"db.php?altr=" . urlencode($artist->altr);
        if ( "$lltr" <> "" ) {
          echo "&lltr=" . urlencode($lltr);
        }
        if ( "$a" <> "" ) {
          echo "&a=" . urlencode($a);
        }
        elseif ( "$l" <> "" ) {
          echo "&l=" . urlencode($l);
        }
        echo "\">$artist->altr</a>\n";
      }
    }
    echo "</p>\n<p>";
    if ( isset($altr) ) {
      $qa = "SELECT distinct artist FROM tracks WHERE artist ilike '$altr%' ORDER BY 1;";
      $res = awm_pgexec( $dbconn, $qa, "db");
      if ( $res && pg_NumRows($res) > 0 ) {
        // echo "<p><b>ARTISTS &nbsp;&gt; &gt;</b> &nbsp;\n";
        for ( $i = 0; $i < pg_NumRows($res); $i++ ) {
          $artist = pg_Fetch_Object( $res, $i );
          if ( $i > 0 ) echo " - ";
          echo "<a href=\"db.php?a=" . urlencode($artist->artist) . "$letter_get\">[" . str_replace(" ", " ", $artist->artist) . "]</a>\n";
        }
      }
    }
    echo "</td>\n";

    echo "<td width=" . ((100 - $widthused) / 2) . "%>\n";
    echo "<h3>Albums</h3>\n<p>";
    $qa = "SELECT distinct upper(substring(album from 1 for 1)) AS lltr FROM tracks ORDER BY 1;";
    $res = awm_pgexec( $dbconn, $qa, "db");
    if ( $res && pg_NumRows($res) > 0 ) {
      for ( $i = 0; $i < pg_NumRows($res); $i++ ) {
        $artist = pg_Fetch_Object( $res, $i );
        if ( "$artist->lltr" == "" ) continue;
        echo "<a class=alphabetica href=\"db.php?lltr=" . urlencode($artist->lltr);
        if ( "$altr" <> "" ) {
          echo "&altr=" . urlencode($altr);
        }
        if ( "$a" <> "" ) {
          echo "&a=" . urlencode($a);
        }
        elseif ( "$l" <> "" ) {
          echo "&l=" . urlencode($l);
        }
        echo "\">$artist->lltr</a>\n";
      }
    }
    echo "</h3>\n<p>";
    if ( isset($lltr) ) {
      $qa = "SELECT distinct album FROM tracks WHERE album ilike '$lltr%' ORDER BY 1;";
      $res = awm_pgexec( $dbconn, $qa, "db");
      if ( $res && pg_NumRows($res) > 0 ) {
        echo "<p><b>ALBUMS &nbsp;&gt; &gt;</b> &nbsp;\n";
        for ( $i = 0; $i < pg_NumRows($res); $i++ ) {
          $album = pg_Fetch_Object( $res, $i );
          echo "<a href=\"db.php?l=" . urlencode($album->album) . "$letter_get\">[" . str_replace(" ", " ", $album->album) . "]</a> &nbsp;\n";
        }
      }
    }
    echo "</td>\n";
  echo "</tr></table>\n";
  show_queue();
?>

</body>
</html>
