<?php
  require_once("always.php");
  require_once("Session.php");

  param_to_global('a','{.*}' );
  param_to_global('l','{.*}' );

  require_once("PlayTracks.php");

  $title = $c->system_name;
  require_once("header.php");


  if ( "$error_loc$error_msg" == "" ) {

function track_link( $trk, $link_title, $row_class = "" ) {
  global $letter_get, $search;

  $track_link_url = sprintf( "?l=%s&a=%s&t=%s", urlencode($trk->album), urlencode($trk->artist), urlencode($trk->title) );
  $safe_title = htmlspecialchars($link_title);
  if ( isset($search) ) {
    $delimiter_list = '/#~`!@$%^&*_-=|';
    for( $i=0; strpos($search,substr($delimiter_list,$i,1)) !== false; $i++ );
    $delimiter = substr($delimiter_list,$i,1);
    if ( preg_match($delimiter.$search.$delimiter.'i', $link_title) ) {
      $row_class = "found";
    }
  }

  $duration = preg_replace( "/^[0:]{1,4}/", "", $trk->duration );
  $last_played = ($trk->last_played == '-infinity'?"-":substr($trk->last_played,0,10));
  $link = <<<EOHTML
<tr class="track$row_class">
  <td class="track$row_class">
    <a class="track$row_class" href="artist.php$track_link_url$letter_get" title="$trk->path_name">$safe_title</a>
  </td>
  <td class="track$row_class"> $last_played </td>
  <td class="track$row_class"> $duration </td>
  <th class="track$row_class">
    <a class="alphabetica" href="edit_track.php$track_link_url" title="Edit Track Info">E</a>
  </th>
</tr>

EOHTML;

  return $link;
}


    echo "<table width=\"100%\"><tr valign=\"top\">\n";

    echo "<td width=\"50%\">\n";
    if ( isset($a) ) {
      $qa = "SELECT artist, album, title, tracknum, path_name, duration::interval(0), ";
      $qa .= "extract( EPOCH FROM duration)::int AS secs, quality, get_last_played(hash_key) AS last_played ";
      $qa .= "FROM tracks WHERE lower(artist) = lower(?) ";
      $qa .= "ORDER BY lower(album), setpart, tracknum; ";
      $qry = new AwlQuery($qa, $a);
      if ( $qry->Exec() && $qry->rows() > 0 ) {
        echo "<h3>".htmlspecialchars($a)."</h3>\n";
        $last_album = "";
        $rownum = 0;
        while ( $track = $qry->Fetch() ) {
          if ( $track->album != "" && $track->album != $last_album ) {
            if ( $rownum > 1 ) echo "</table>";
            echo "<br><table width=100% cellspacing=0 cellpadding=0><tr class=th4><td width=71% class=th4>";
            echo "<a class=th4 href=\"artist.php?a=" . urlencode($track->artist) . "&l=" . urlencode($track->album) . "$letter_get\">".htmlspecialchars($track->album)."</a></td>\n";
            echo "<td width=15% class=th4>&nbsp;</td>";
            echo "<td width=10% class=th4>&nbsp;</td>";
            echo "<td width=4% class=th4><a class=alphabetica  class=track href=\"edit_track.php?l=" . urlencode($track->album) . "&a=" . urlencode($track->artist) . "\" title=\"Edit Album Info\">E</a></td></tr>";
            $last_album = $track->album;
          }
          else if ( $rownum == 0 ) {
            echo "<br><table width=100% cellspacing=0 cellpadding=0><tr class=th4><td width=71% class=th4>Unkown Album</td>";
            echo "<td width='29%' class='th4' colspan='3'>&nbsp;</td></tr>\n";
          }
          echo track_link($track, ($track->tracknum > 0 ? "$track->tracknum: " : "") . "$track->title",  $rownum % 2 );
          $rownum++;
        }
        echo "</table>";
      }
    }
    else if ( isset( $l ) ) {
      $qa = "SELECT artist, album, title, tracknum, path_name, duration::interval(0), ";
      $qa .= "extract( EPOCH FROM duration)::int AS secs, quality, get_last_played(hash_key) AS last_played ";
      $qa .= "FROM tracks WHERE album = '" . addslashes($l) . "' ";
      $qa .= "ORDER BY album, setpart, tracknum; ";
      $qry = new AwlQuery($qa);
      if ( $qry->Exec() && $qry->rows() > 0 ) {
        echo "<h3><a class=th4 href=\"artist.php?play=1&l=" . urlencode($l) . "$letter_get\">$l</a></h3>\n";
        $last_artist = "";
        while ( $track = $qry->Fetch() ) {
          if ( $track->artist != "" && $track->artist != $last_artist ) {
            echo "<br><table width=100% cellspacing=0 cellpadding=0><tr class=th4><td width=71% class=th4>";
            echo "<a class=h4 href=\"artist.php?a=" . urlencode($track->artist) . "&l=" . urlencode($track->album) . "$letter_get\">$track->artist</a></td>\n";
            echo "<td width=15% class=th4>&nbsp;</td>";
            echo "<td width=10% class=th4>&nbsp;</td>";
            echo "<td width=4% class=th4><a class=alphabetica  class=track href=\"edit_track.php?l=" . urlencode($track->album) . "&a=" . urlencode($track->artist) . "\" title=\"Edit Album Info\">E</a></td></tr>";
            $last_artist = $track->artist;
          }
          else if ( $rownum == 0 ) {
            echo "<br><table width=100% cellspacing=0 cellpadding=0><tr class=th4><td width=71% class=th4>Unkown Album</td>";
            echo "<td width='29%' class='th4' colspan='3'>&nbsp;</td></tr>\n";
          }
          echo track_link($track, ($track->tracknum > 0 ? "$track->tracknum: " : "") . "$track->title",  $rownum % 2 );
          $rownum++;
        }
        echo "</table>";
      }
    }
    else {
      echo "&nbsp;";
    }
    echo "</td>";

    echo "<td width=\"50%\">\n";
    echo "<h3>Artists</h3>\n<p>";
    if ( isset($altr) || isset($search) ) {
      $altr = trim("$altr");
      if ( isset($search) ) {
        $sql = "SELECT distinct ON ( lower(artist)) artist FROM tracks ";
        $sql .= "WHERE artist ~* ? OR album ~* ? OR title ~* ? OR path_name ~* ? ";
        $sql .= "ORDER BY lower(artist);";
        $qry = new AwlQuery( $sql, $search, $search, $search, $search );
      }
      else {
        $qry = new AwlQuery( "SELECT distinct ON ( lower(artist)) artist FROM tracks WHERE artist ~* ? ORDER BY lower(artist)", "^$altr");
      }
      if ( $qry->Exec('artist') && $qry->rows() > 0 ) {
        while ( $artist = $qry->Fetch() ) {
          $display = htmlspecialchars($artist->artist);
          if ( trim($display) == "" ) $display = "&laquo;unknown&raquo;";
          echo " <a href=\"artist.php?a=" . urlencode($artist->artist) . "$letter_get\" class=\"artist\">$display</a>\n";
        }
      }
    }
    echo "</td>\n";

    echo "</tr></table>\n";
  }

  include("footers.php");

