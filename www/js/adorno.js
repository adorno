// Javascript for Adorno Music Player

var base_url = /(https?:\/\/[a-z0-9.-]+\/)/i.exec(document.URL)
base_url = base_url[0];


////////////////////////////////////////////////////////////////
// Toggle the Artist/Album/Genre/Stream selection
////////////////////////////////////////////////////////////////
function toggle_type() {
  var type_span = document.getElementById('current_type');
  var type = type_span.innerHTML.toLowerCase();
  var old_type = type;

  if ( type == 'artist' )       { type = 'album'; }
  else if ( type == 'album' )   { type = 'stream'; }
  else if ( type == 'stream' )  { type = 'genre'; }
  else if ( type == 'genre' )   { type = 'artist'; }

  type_span.innerHTML = type.substring(0,1).toUpperCase() + type.substring(1);

  var alphabet = document.getElementById('alphabet').childNodes;
  
  var href;
  var title;
  var title_match = new RegExp('([0-9]+ )?' + old_type,'g');
 
  for (var i = 0; i < alphabet.length; i++) {
    href = alphabet[i].href;
    alphabet[i].href = href.replace(old_type,type);
//    if ( i == 2 ) {
//      alert( href + "\n" + alphabet[i].href );
//    }
    title = alphabet[i].title;
    alphabet[i].title = title.replace(title_match,type);
  }

  var search_form = document.getElementById('search_form');
  for (var i=0; i < search_form.elements.length; i++ ) {
    if ( search_form.elements[i].name == 'type' ) {
      search_form.elements[i].value = type;
      break;
    }
  }
}


var status_req = new XMLHttpRequest();
var mytime;
var redo_queue = true;

function update_now_playing() {
  var td = document.getElementById('now_playing');
  status_req.open('GET', base_url + 'current.php', true);
  status_req.onreadystatechange = function (aEvt) {
    if ( status_req.readyState == 4 ) {
      if ( status_req.status == 200 ) {
        if ( td.innerHTML != status_req.responseText ) {
          td.innerHTML = status_req.responseText;
          var in_td = td.childNodes;
          var title = '';
          if ( in_td.length == 1 ) {
            var nodecontent = status_req.responseText;
            if ( nodecontent.match(/Nothing is currently playing/i) ) {
              title = 'Adorno: stopped';
            }
            else if ( nodecontent.match(/paused/i) ) {
              title = 'Adorno: paused';
            }
            in_td = in_td[0].childNodes;
          }
          for ( var i=0; i < in_td.length; i++ ) {
            if ( in_td[i].innerHTML == undefined ) continue;
            if ( title == '' )
              title = in_td[i].innerHTML;
            else
              title = title + ' : ' + in_td[i].innerHTML;
          }
          document.title = title;
          update_queue();
        }
        else if ( redo_queue )
          update_queue();
      }
    }
  };
  status_req.send(null);
  mytime=setTimeout('update_now_playing()',10000);
}

function update_queue() {
  var td = document.getElementById('queue');
  redo_queue = false;
  status_req.open('GET', base_url + 'queue.php', true);
  status_req.onreadystatechange = function (aEvt) {
    if ( status_req.readyState == 4 ) {
      if ( status_req.status == 200 ) {
        td.innerHTML = status_req.responseText;
      }
      else
        redo_queue = true;
    }
  };
  status_req.send(null);
}


var last_action_result = '';
function act() {
  var argv = act.arguments;
  var argc = argv.length;

  if ( argc < 1 || argc > 2 ) {
    alert( 'act(action,details) - only 1 or two arguments, not ' + argc + ' please!' );
    return;
  }

  var action = argv[0];
  var details = '';
  if ( argc == 2 ) details = argv[1];

  var act_req = new XMLHttpRequest();
  status_req.open('GET', base_url + 'action.php?action=' + action + details, true);
  status_req.onreadystatechange = function (aEvt) {
    if ( status_req.readyState == 4 ) {
      if ( status_req.status == 200 ) {
        last_action_result = 'success';
        redo_queue = true;
        update_now_playing();
      }
      else {
        redo_queue = true;
        last_action_result = 'failure';
      }
    }
  };
  status_req.send(null);
}


function enqueue(artist,album,track) {
  var details = '&l=' + album;
  if ( artist != '' ) details = details + '&a=' + artist;
  if ( track != '' ) details = details + '&t=' + track;
  act('enqueue',details);
}


mytime=setTimeout('update_now_playing()',1000);
