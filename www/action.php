<?php
  include('always.php');

  header('Content-type: text/plain');

  // echo "Action=" . $action, "\n";
  switch( $action ) {
    case 'enqueue':
      if ( isset($url) ) {
        PlayTrack( $url, md5($url) );

        // Attempt to update the database with the stream details
        $sql = "SELECT 1 FROM streams WHERE stream_url = ?";
        $qry = new AwlQuery( $sql, $url );
        if ( $qry->rows() == 0 ) {
          $sql = "INSERT INTO streams ( stream_url ) VALUES( ? )";
          $qry = new AwlQuery( $sql, $url );
        }
        // echo "Playing $url\n";
      }
      else {
        $sql = 'SELECT path_name, hash_key FROM tracks WHERE album = :album ';
        $params = array( ':album' => $l );
        if ( isset($a)  && $a != '' ) {
          $sql .= 'AND artist = :artist ';
          $params[':artist'] = $a;
        }
        if ( isset($t)  && $t != '' ) {
          $sql .= 'AND title = :title ';
          $params[':title'] = $t;
        }
        $sql .= 'ORDER BY setpart, tracknum';
        echo $sql;
        print_r( $params );

        $qry = new AwlQuery( $sql, $params );
        if ( $qry->Exec("PlayTracks") && $qry->rows() > 0 ) {
          while( $track = $qry->Fetch() ) {
            PlayTrack( $track->path_name, $track->hash_key );
            // echo "Playing $track->path_name\n";
          }
        }
      }
      break;


    case 'move':
      param_to_global( 'i', 'int' );
      param_to_global( 'd', '{^[ud]}' );
      daemon_move( $i, (substr($d,0,1) == 'd' ? 1 : -1) );
      break;


    default:
      daemon_other_command($action,null);
      break;

  }
