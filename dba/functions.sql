
CREATE or REPLACE FUNCTION get_last_played( TEXT ) RETURNS TIMESTAMP AS '
DECLARE
  hash ALIAS FOR $1;
  last_time TIMESTAMP;
BEGIN
  -- ORDER BY hash_key and req_at makes index choice more sane.
  SELECT req_at INTO last_time FROM played WHERE hash_key=hash
                   ORDER BY hash_key DESC, req_at DESC LIMIT 1;
  IF NOT FOUND THEN
    last_time := ''-infinity'';
  END IF;
  RETURN last_time;
END;
' LANGUAGE 'plpgsql' STRICT;
