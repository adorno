#!/usr/bin/make -f
# -*- makefile -*-
# debian/rules file - for whereami
# Copyright 1999-2001 by Andrew McMillan
# Based on the pcmcia package by Brian Mays
# Patterned after the hello package by Ian Jackson.

# The package we are dealing with
package=whereami

# Uncomment this to turn on verbose mode.
export DH_VERBOSE=1

# This is the debhelper compatability version to use.
export DH_COMPAT=3


build-stamp:
	dh_testdir
	umask 022; $(MAKE) all
	touch build-stamp

clean:
	dh_testdir
	dh_testroot
	rm -f build-stamp
	-$(MAKE) clean
	dh_clean

binary:	build-stamp
	dh_testdir
	dh_testroot
	dh_clean -k
	$(RM) -r debian/$(package)
	dh_installdebconf
# Install directories
	dh_installdirs DEBIAN etc etc/init.d etc/$(package) \
							etc/$(package)/tests etc/apm etc/apm/event.d \
							usr usr/sbin usr/share usr/share/doc \
							usr/share/$(package) \
							usr/share/$(package)/tests \
							usr/share/doc/$(package) \
							usr/share/doc/$(package)/html/ \
							usr/share/doc/$(package)/html/images \
							usr/share/doc/$(package)/patches/ \
							usr/share/doc/$(package)/examples/ \
							usr/share/man usr/share/man/man8

# Install files
	install bin/$(package).sh debian/$(package)/usr/sbin/.
	install bin/$(package).pl debian/$(package)/usr/sbin/.
	dh_installinit --no-restart-on-upgrade --update-rcd-params="defaults 35 65"
	install bin/whereami.apm debian/$(package)/etc/apm/event.d/10$(package)
	install -m644 whereami.conf mail-relay.conf apm.conf detect.conf debian/$(package)/etc/$(package)/.
	install -m644 debian/*.patch debian/$(package)/usr/share/doc/$(package)/patches/.
	install -m644 `find samples -maxdepth 1 -type f` debian/$(package)/usr/share/doc/$(package)/examples/.
	install -m644 html/*.html debian/$(package)/usr/share/doc/$(package)/html/.
	install -m644 `find html/images -maxdepth 1 -type f` debian/$(package)/usr/share/doc/$(package)/html/images/.
	install -m755 `find scripts -maxdepth 1 -type f ` debian/$(package)/usr/share/$(package)/.
	install -m755 `find scripts/tests -maxdepth 1 -type f ` debian/$(package)/usr/share/$(package)/tests/.
# Find configuration files
	find debian/$(package)/etc/$(package) -name "*.conf" -not -type d | \
		sed 's%debian/$(package)%%' > debian/$(package)/DEBIAN/conffiles
	echo "/etc/init.d/$(package)" >>debian/$(package)/DEBIAN/conffiles
	echo "/etc/apm/event.d/10$(package)" >>debian/$(package)/DEBIAN/conffiles
# Install documentation
	install -m644 AUTHORS README CONFIGURE VERSION TODO \
	  debian/$(package)/usr/share/doc/$(package)/.
	dh_installman man/whereami.8 man/detect.conf.5 man/whereami.conf.5
	(cd debian/$(package)/usr/share/man/man8; ln -s whereami.8 whereami.pl.8; ln -s whereami.8 whereami.sh.8)
	dh_installchangelogs
	dh_compress
	install -m644 debian/copyright \
	  debian/$(package)/usr/share/doc/$(package)/copyright
	dh_fixperms
	# Set permissions on our helper scripts
	chmod 755 debian/$(package)/usr/share/$(package)/*
	dh_installdeb
	dh_perl
	dh_gencontrol
	sed '/:/s/, *$$//' debian/$(package)/DEBIAN/control \
	  >debian/$(package)/DEBIAN/control.new
	mv debian/$(package)/DEBIAN/control.new debian/$(package)/DEBIAN/control
	dh_md5sums
	dh_builddeb


# Below here is fairly generic really

binary:		binary-indep binary-arch

source diff:
	@echo >&2 'source and diff are obsolete - use dpkg-source -b'; false

.PHONY: binary-arch binary-indep clean
