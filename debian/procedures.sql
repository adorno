
-- Update some information about the track having been played.
-- This is kind of trivial for now, but we expect to see more logic in
-- here eventually.
CREATE or REPLACE FUNCTION track_played( TEXT ) RETURNS TEXT AS '
  DECLARE
    in_hash ALIAS FOR $1;
  BEGIN
    INSERT INTO played ( hash_key, req_at ) VALUES( in_hash , current_timestamp );
    RETURN  in_hash;
  END;
' LANGUAGE 'plpgsql';


CREATE or REPLACE FUNCTION translation_of( TEXT ) RETURNS TEXT AS '
  DECLARE
    in_text ALIAS FOR $1;
    out_text TEXT;
  BEGIN
    SELECT translated INTO out_text FROM translations WHERE in_text ~* original ;
    IF NOT FOUND THEN
      RETURN in_text;
    END IF;
    RETURN out_text;
  END;
' LANGUAGE 'plpgsql';

CREATE or REPLACE FUNCTION fix_artists() RETURNS INT AS '
             update tracks set artist = translation_of(artist) where artist != translation_of(artist);
             SELECT 1;
' LANGUAGE sql;