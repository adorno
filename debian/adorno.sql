--- SQL Database definitions for Adorno

CREATE TABLE tracks (
  hash_key TEXT,
  path_name TEXT,
  artist TEXT,
  album TEXT,
  title TEXT,
  tracknum INT4,
  duration INTERVAL,
  quality TEXT
);
CREATE INDEX tracks_artist_lower ON tracks ( lower(artist) );


CREATE TABLE streams (
  stream_url TEXT,
  playlist_url TEXT,
  genre TEXT,
  bitrate TEXT,
  format TEXT,
  description TEXT
);

CREATE TABLE played (
  hash_key TEXT,
  req_at TIMESTAMP
);

CREATE TABLE translations (
  original TEXT PRIMARY KEY,
  translated TEXT
);

CREATE TABLE genres (
  id SERIAL PRIMARY KEY,
  mp3_id INT4,
  description TEXT
);

CREATE TABLE classifications (
  genre INT4 REFERENCES genres(id),
  hash_key TEXT,
  PRIMARY KEY ( genre, hash_key )
);

-- Old hashes
CREATE TABLE old_hashes (
  hash_key TEXT,
  path_name TEXT
);

-- Duplicate tracks
CREATE TABLE duplicates (
  hash_key TEXT,
  count INT
);

CREATE TABLE letter_groups (
  letter text primary key,
  pattern text,
  artist_count int,
  album_count int
);

GRANT INSERT, UPDATE, SELECT ON
    tracks,
    streams,
    played,
    classifications,
    duplicates,
    genres,
    letter_groups
    TO general;

\i procedures.sql
